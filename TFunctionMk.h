#pragma once
#include "TFunctions.h"

class TFunctionMk5 :
	public TFunctions
{
public:
	TFunctionMk5() {}
	~TFunctionMk5() {}
	void run();
};

class TFunctionMk4 :
	public TFunctions
{
public:
	TFunctionMk4() {}
	~TFunctionMk4() {}
	void run();
};

class TFunctionMk3 :
	public TFunctions
{
public:
	TFunctionMk3() {}
	~TFunctionMk3() {}
	void run();
};

class TFunctionMk2 :
	public TFunctions
{
public:
	TFunctionMk2() {}
	~TFunctionMk2() {}
	void run();
};

class TFunctionMk1 :
	public TFunctions
{
public:
	TFunctionMk1() {}
	~TFunctionMk1() {}
	void run();
};

class TFunctionMk :
	public TFunctions
{
public:
	TFunctionMk();
	~TFunctionMk();
	void run();
};

