#pragma once
#include "TFunctions.h"

class TFunctionSh5 :
	public TFunctions
{
public:
	TFunctionSh5(){}
	~TFunctionSh5(){}
	void run();
};

class TFunctionSh4 :
	public TFunctions
{
public:
	TFunctionSh4() {}
	~TFunctionSh4() {}
	void run();
};

class TFunctionSh3 :
	public TFunctions
{
public:
	TFunctionSh3() {}
	~TFunctionSh3() {}
	void run();
};

class TFunctionSh2 :
	public TFunctions
{
public:
	TFunctionSh2() {}
	~TFunctionSh2() {}
	void run();
};

class TFunctionSh1 :
	public TFunctions
{
public:
	TFunctionSh1() {}
	~TFunctionSh1() {}
	void run();
};

class TFunctionSh :
	public TFunctions
{
public:
	TFunctionSh();
	~TFunctionSh();
	void run();
};

