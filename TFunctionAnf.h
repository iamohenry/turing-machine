#pragma once
#include "TFunctions.h"

class TFunctionAnf1 :
	public TFunctions
{
public:
	TFunctionAnf1() {}
	~TFunctionAnf1() {}
	void run();
};

class TFunctionAnf :
	public TFunctions
{
public:
	TFunctionAnf();
	~TFunctionAnf();
	void run();
};

