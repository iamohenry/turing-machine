#include "TFunctionCon.h"

TFunctionCon::TFunctionCon()
{
}


TFunctionCon::~TFunctionCon()
{
}

void TFunctionCon::prepareTestSample1()
{
	m_cells[115] = 'D';
	m_cells[117] = 'A';
	m_cells[119] = 'D';
	m_cells[121] = 'D';
	m_cells[123] = 'C';
	m_cells[125] = 'R';
}

void TFunctionCon::prepareTestSample2()
{
	m_cells[115] = 'D';
	m_cells[117] = 'A';
	m_cells[119] = 'A';
	m_cells[121] = 'D';
	m_cells[123] = 'D';
	m_cells[125] = 'C';
	m_cells[127] = 'C';
	m_cells[129] = 'R';
	m_cells[131] = 'D';
	m_cells[133] = 'A';
}

void TFunctionCon::prepareTestSample3()
{
	m_cells[123] = 'D';
	m_cells[125] = 'C';
	m_cells[127] = 'C';
	m_cells[129] = 'R';
	m_cells[131] = 'D';
	m_cells[133] = 'A';
}

void TFunctionCon::run()
{
	TFunctionCon1 tfCon1;
	tfCon1.setFunc(tf_func);
	tfCon1.setSearchA(tf_searchA);

	while (!matchSth('A')) {
		R();
		sendUpdateSignal();
		R();
		sendUpdateSignal();
	}
	if (matchSth('A')) {
		L();
		sendUpdateSignal();
		prSth(tf_searchA);
		sendUpdateSignal();
		R();
		sendUpdateSignal();
		tfCon1.run();
	}
}

void TFunctionCon::test(int order)
{
	reset();
	m_index = 113;
	switch (order) {
	case 1:		
		prepareTestSample1();
		break;
	case 2:
		prepareTestSample2();
		break;
	case 3:
		prepareTestSample3();
		break;
	default:
		break;
	}
	run();
	sendUpdateSignal();
}

void TFunctionCon1::run()
{
	TFunctionCon2 tfCon2;
	tfCon2.setFunc(tf_func);
	tfCon2.setSearchA(tf_searchA);

	while (matchSth('A')) {
		R();
		sendUpdateSignal();
		prSth(tf_searchA);
		sendUpdateSignal();
		R();
		sendUpdateSignal();
	}

	if (matchSth('D')) {
		R();
		sendUpdateSignal();
		prSth(tf_searchA);
		sendUpdateSignal();
		R();
		sendUpdateSignal();
		tfCon2.run();
	}
	else if (matchSth(0)) {
		prSth('D');
		sendUpdateSignal();
		R();
		sendUpdateSignal();
		prSth(tf_searchA);
		for (int i = 0; i < 3; i++) {
			R();
			sendUpdateSignal();
		}
		if (tf_func) {
			TFunctions* tf = static_cast<TFunctions*>(tf_func);
			tf->run();
		}
	}
}

void TFunctionCon2::run()
{
	while (matchSth('C')) {
		R();
		sendUpdateSignal();
		prSth(tf_searchA);
		sendUpdateSignal();
		R();
		sendUpdateSignal();
	}
	if (!matchSth('C')) {
		R();
		sendUpdateSignal();
		R();
		sendUpdateSignal();
		if (tf_func) {
			TFunctions* tf = static_cast<TFunctions*>(tf_func);
			tf->run();
		}
	}
}
