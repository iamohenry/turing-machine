#include "TFunctionRE.h"
#include "TFunctionF.h"



TFunctionRE::TFunctionRE()
{
}


TFunctionRE::~TFunctionRE()
{
}

void TFunctionRE::prepareTestSample1()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = '0';
	m_cells[120] = 'a';
	m_cells[121] = 'x';
	m_cells[122] = 'a';
	m_cells[123] = 'y';
	m_cells[124] = 'a';
}

void TFunctionRE::run()
{
	TFunctionF tfF;
	TFunctionRE1 tfRE1;
	tfRE1.setFunc(tf_func);
	tfRE1.setFunb(tf_funb);
	tfRE1.setSearchA(tf_searchA);
	tfRE1.setReplaceB(tf_replaceB);
	tfF.setFunc(&tfRE1);
	tfF.setFunb(tf_funb);
	tfF.setSearchA(tf_searchA);
	tfF.run();
}

void TFunctionREA::prepareTestSample1()
{
	TFunctionRE tfRE;
	tfRE.prepareTestSample1();
}

void TFunctionREA::run()
{
	TFunctionRE tfRE;
	tfRE.setFunc(this);
	tfRE.setFunb(tf_funb);
	tfRE.setSearchA(tf_searchA);
	tfRE.setReplaceB(tf_replaceB);
	tfRE.run();
}

void TFunctionRE1::run()
{
	E();
	sendUpdateSignal();
	prSth(tf_replaceB);
	sendUpdateSignal();
	if (tf_func) {
		TFunctions* tf = static_cast<TFunctions*>(tf_func);
		tf->run();
	}
}
