#pragma once
#include "TFunctions.h"
class TFunctionCE :
	public TFunctions
{
public:
	TFunctionCE();
	~TFunctionCE();
	void prepareTestSample1();
	void run();
};

class TFunctionCEA :
	public TFunctions
{
public:
	TFunctionCEA() {}
	~TFunctionCEA() {}
	void prepareTestSample1();
	void run();
};

class TFunctionCEA2:  //copy all characters marked with alpha, then beta, finally erase all marks;
	public TFunctions
{
public:
	TFunctionCEA2() {}
	~TFunctionCEA2() {}
	void prepareTestSample1();
	void run();
	void test(int order);
};

class TFunctionCEA3 :  //copy all characters marked with alpha, then beta, at last gama, finally erase all marks;
	public TFunctions
{
public:
	TFunctionCEA3() {}
	~TFunctionCEA3() {}
	void prepareTestSample1();
	void run();
	void test(int order);
};

class TFunctionCEA4 :  //copy all characters marked with alpha, then beta, at last gama, finally erase all marks;
	public TFunctions
{
public:
	TFunctionCEA4() {}
	~TFunctionCEA4() {}
	void prepareTestSample1();
	void run();
	void test(int order);
};

class TFunctionCEA5 :  //copy all characters marked with alpha, then beta, at last gama, finally erase all marks;
	public TFunctions
{
public:
	TFunctionCEA5() {}
	~TFunctionCEA5() {}
	void prepareTestSample1();
	void run();
	void test(int order);
};
