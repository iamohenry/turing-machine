#include "TFunctionTD.h"
#include "qdebug.h"


TFunctionTD::TFunctionTD()
{
}


TFunctionTD::~TFunctionTD()
{
}

void TFunctionTD::run()
{
	qDebug() << "func TD is starting...";
	reset();
	m_config = eConfig_TD_b;
	do {
		switch (m_config)
		{
		case eConfig_TD_b: {
			m_cells[m_index] = 'e';
			sendUpdateSignal();
			m_index++;
			sendUpdateSignal();
			m_cells[m_index] = 'e';
			sendUpdateSignal();
			m_index++;
			sendUpdateSignal();
			m_cells[m_index] = '0';
			sendUpdateSignal();
			m_index++;
			sendUpdateSignal();
			m_index++;
			sendUpdateSignal();
			m_cells[m_index] = '0';
			sendUpdateSignal();
			m_index--;
			sendUpdateSignal();
			m_index--;
			sendUpdateSignal();
			m_config = eConfig_TD_c;
		}
			break;
		case eConfig_TD_c: {
			if (m_cells[m_index] == '1') {
				m_index++;
				sendUpdateSignal();
				m_cells[m_index] = 'x';
				sendUpdateSignal();
				m_index--;
				sendUpdateSignal();
				m_index--;
				sendUpdateSignal();
				m_index--;
				sendUpdateSignal();
			}
			else if (m_cells[m_index] == '0') {
				m_config = eConfig_TD_q;
			}
		}
			break;
		case eConfig_TD_q: {
			if (0 == m_cells[m_index]) {
				m_cells[m_index] = '1';
				sendUpdateSignal();
				m_index--;
				sendUpdateSignal();
				m_config = eConfig_TD_p;
			}
			else if (m_cells[m_index] == '0' || m_cells[m_index] == '1') {
				m_index++;
				sendUpdateSignal();
				m_index++;
				sendUpdateSignal();
			}
		}
			break;
		case eConfig_TD_p: {
			if ('x' == m_cells[m_index]) {
				m_cells[m_index] = 0;
				sendUpdateSignal();
				m_index++;
				sendUpdateSignal();
				m_config = eConfig_TD_q;
			}
			else if ('e' == m_cells[m_index]) {
				m_index++;
				sendUpdateSignal();
				m_config = eConfig_TD_f;
			}
			else if (0 == m_cells[m_index]) {
				m_index--;
				sendUpdateSignal();
				m_index--;
				sendUpdateSignal();
			}
		}
			break;
		case eConfig_TD_f: {
			if (0 == m_cells[m_index]) {
				m_cells[m_index] = '0';
				sendUpdateSignal();
				m_index--;
				sendUpdateSignal();
				m_index--;
				sendUpdateSignal();
				m_config = eConfig_TD_c;
			}
			else if ('0' == m_cells[m_index] || '1' == m_cells[m_index]) {
				m_index++;
				sendUpdateSignal();
				m_index++;
				sendUpdateSignal();
			}
		}
			break;
		default:
			break;
		}
	} while (m_index < 200);
}
