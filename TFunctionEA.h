#pragma once
#include "TFunctions.h"
class TFunctionEA :
	public TFunctions
{
public:
	TFunctionEA();
	~TFunctionEA();
	void run();
	void prepareTestSample1();
	void prepareTestSample2();
	void prepareTestSample3();
	void prepareTestSample4();
	void prepareTestSample5();
};

