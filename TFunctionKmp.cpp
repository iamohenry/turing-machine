#include "TFunctionKmp.h"
#include "TFunctionCPE.h"
#include "TFunctionEA.h"
#include "TFunctionSim.h"
#include "TFunctionAnf.h"



TFunctionKmp::TFunctionKmp()
{
}


TFunctionKmp::~TFunctionKmp()
{
}

void TFunctionKmp::run()
{
	TFunctionAnf tfAnf;
	TFunctionSim tfSim;
	TFunctionEA tfEA_ext;
	TFunctionEA tfEA_inn;
	tfEA_inn.setFunb(&tfAnf);
	tfEA_inn.setSearchA('x');
	tfEA_ext.setFunb(&tfEA_inn);
	tfEA_ext.setSearchA('y');

	TFunctionCPEA tfCPEA;
	tfCPEA.setFunc(&tfEA_ext);
	tfCPEA.setFunb(&tfSim);
	tfCPEA.setSearchA('x');
	tfCPEA.setReplaceB('y');
	tfCPEA.run();
}
