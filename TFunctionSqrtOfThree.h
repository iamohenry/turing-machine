#pragma once
#include "TFunctions.h"
class TFunctionSqrtOfThree :
	public TFunctions
{
public:
	TFunctionSqrtOfThree();
	~TFunctionSqrtOfThree();
	void run();
private:
	double calResult();	
private:
	enum eConfig_SqrtOfThree {
		eConfig_SqrtOfThree_begin,
		eConfig_SqrtOfThree_new,
		eConfig_SqrtOfThree_mark_digits,
		eConfig_SqrtOfThree_find_x,
		eConfig_SqrtOfThree_first_r,
		eConfig_SqrtOfThree_last_r,
		eConfig_SqrtOfThree_find_digits,
		eConfig_SqrtOfThree_find_1st_digit,
		eConfig_SqrtOfThree_found_1st_digit,
		eConfig_SqrtOfThree_find_2nd_digit,
		eConfig_SqrtOfThree_found_2nd_digit,
		eConfig_SqrtOfThree_add_zero,
		eConfig_SqrtOfThree_add_one,
		eConfig_SqrtOfThree_carry,
		eConfig_SqrtOfThree_add_finished,
		eConfig_SqrtOfThree_erase_old_x,
		eConfig_SqrtOfThree_print_new_x,
		eConfig_SqrtOfThree_erase_old_y,
		eConfig_SqrtOfThree_print_new_y,
		eConfig_SqrtOfThree_find_tbd_digit,
		eConfig_SqrtOfThree_find_last_r,
		eConfig_SqrtOfThree_check_res,
		eConfig_SqrtOfThree_check_2_nd_digit,
		eConfig_SqrtOfThree_reset_new_x,
		eConfig_SqrtOfThree_flag_result_digits,
		eConfig_SqrtOfThree_unflag_result_digits,
		eConfig_SqrtOfThree_new_digit_is_zero,
		eConfig_SqrtOfThree_print_zero_digit,
		eConfig_SqrtOfThree_new_digit_is_one,
		eConfig_SqrtOfThree_print_one_digit,
		eConfig_SqrtOfThree_cleanup,
	};
	eConfig_SqrtOfThree m_config;
};

