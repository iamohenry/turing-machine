#include "TFunctionCPE.h"
#include "TFunctionCP.h"
#include "TFunctionE.h"


TFunctionCPE::TFunctionCPE()
{
}


TFunctionCPE::~TFunctionCPE()
{
}

void TFunctionCPE::init(TFunctions * func, 
	TFunctions * funb, 
	TFunctions * fund,
	char alpha, 
	char beta)
{
	setFunc(func);
	setFunb(funb);
	setFund(fund);
	setSearchA(alpha);
	setReplaceB(beta);
}

void TFunctionCPE::test(int order)
{
	reset();
	switch (order) {
	case 1:
		prepareTestSample1();
		break;
	case 2:
		prepareTestSample2();
		break;
	case 3:
		prepareTestSample3();
		break;
	case 4:
		prepareTestSample4();
		break;
	default:
		break;
	}	
	run();
	sendUpdateSignal();
}

void TFunctionCPE::prepareTestSample1()
{
	prepareTestSample("ee1a1b");
}

void TFunctionCPE::prepareTestSample2()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = '0';
	m_cells[120] = 'a';
	m_cells[121] = '1';
	m_cells[122] = 'b';
	m_cells[123] = '0';
	m_cells[124] = 'b';
}

void TFunctionCPE::prepareTestSample3()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = '0';
	m_cells[120] = 'b';
}

void TFunctionCPE::prepareTestSample4()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '0';
	m_cells[118] = 'a';
	m_cells[119] = '1';
	m_cells[120] = 'a';
	m_cells[121] = '1';
	m_cells[122] = 'b';
	m_cells[123] = '1';
	m_cells[124] = 'b';
}

void TFunctionCPE::run()
{
	TFunctionCP tfCP;
	TFunctionE tfE_ext;
	TFunctionE tfE_in;

	tfE_in.setFunc(tf_func);
	tfE_in.setFunb(tf_func);
	tfE_in.setSearchA(tf_replaceB);

	tfE_ext.setFunc(&tfE_in);
	tfE_ext.setFunb(tf_func);
	tfE_ext.setSearchA(tf_searchA);

	tfCP.setFunc(&tfE_ext);
	tfCP.setFunb(tf_funb);
	tfCP.setFund(tf_fund);
	tfCP.setSearchA(tf_searchA);
	tfCP.setReplaceB(tf_replaceB);

	tfCP.run();	
}

void TFunctionCPEA::init(TFunctions * func, 
	TFunctions * funb, 
	char alpha, 
	char beta)
{
	setFunc(func);
	setFunb(funb);
	setSearchA(alpha);
	setReplaceB(beta);
}

void TFunctionCPEA::test(int order)
{
	reset();
	switch (order) {
	case 1:
		prepareTestSample1();
		break;
	case 2:
		prepareTestSample2();
		break;
	case 3:
		prepareTestSample3();
		break;
	case 4:
		prepareTestSample4();
		break;
	default:
		break;
	}
	run();
	sendUpdateSignal();
}

void TFunctionCPEA::prepareTestSample1()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = '0';
	m_cells[120] = 'a';
	m_cells[121] = '1';
	m_cells[122] = 'b';
	m_cells[123] = '0';
	m_cells[124] = 'b';
}

void TFunctionCPEA::prepareTestSample2()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = '1';
	m_cells[120] = 'a';
	m_cells[121] = '1';
	m_cells[122] = 'b';
	m_cells[123] = '0';
	m_cells[124] = 'b';
}

void TFunctionCPEA::prepareTestSample3()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '0';
	m_cells[118] = 'a';
	m_cells[119] = '0';
	m_cells[120] = 'a';
	m_cells[121] = '1';
	m_cells[122] = 'b';
	m_cells[123] = '0';
	m_cells[124] = 'b';
}

void TFunctionCPEA::prepareTestSample4()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = '0';
	m_cells[120] = 'a';
	m_cells[121] = '0';
	m_cells[122] = 'a';
	m_cells[123] = '1';
	m_cells[124] = 'a';
	m_cells[125] = '1';
	m_cells[126] = 'b';
	m_cells[127] = '0';
	m_cells[128] = 'b';
	m_cells[129] = '0';
	m_cells[130] = 'b';
	m_cells[131] = '1';
	m_cells[132] = 'b';
}

void TFunctionCPEA::run()
{
	TFunctionCPE tfCPE;
	tfCPE.setFunc(this);
	tfCPE.setFunb(tf_func);
	tfCPE.setFund(tf_funb);
	tfCPE.setSearchA(tf_searchA);
	tfCPE.setReplaceB(tf_replaceB);

	tfCPE.run();
}
