#include "TFunctionInst.h"
#include "TFunctionG.h"
#include "TFunctionF.h"
#include "TFunctionE.h"
#include "TFunctionCE.h"
#include "TFunctionAnf.h"


TFunctionInst::TFunctionInst()
{
}


TFunctionInst::~TFunctionInst()
{
}

void TFunctionInst::run()
{
	TFunctionGII tfGII;
	TFunctionL tfL;
	TFunctionInst1 tfInst1;
	tfL.setFunc(&tfInst1);	
	tfGII.setFunc(&tfL);
	tfGII.setSearchA('u');
	tfGII.run();
}

void TFunctionInst1::run()
{
	TFunctionOv tfOv;
	TFunctionCEA5 tfCEA5;
	tfCEA5.setFunb(&tfOv);
	
	if (matchSth('L')) {
		R();
		sendUpdateSignal();
		E();
		sendUpdateSignal();
		tfCEA5.setSearchA('v');
		tfCEA5.setReplaceB('y');
		tfCEA5.setCharGama('x');
		tfCEA5.setCharDelta('u');
		tfCEA5.setCharE('w');		
		tfCEA5.run();
	}else if (matchSth('R')) {
		R();
		sendUpdateSignal();
		E();
		sendUpdateSignal();
		tfCEA5.setSearchA('v');
		tfCEA5.setReplaceB('x');
		tfCEA5.setCharGama('u');
		tfCEA5.setCharDelta('y');
		tfCEA5.setCharE('w');
		tfCEA5.run();
	}else if (matchSth('N')) {
		R();
		sendUpdateSignal();
		E();
		sendUpdateSignal();
		tfCEA5.setSearchA('v');
		tfCEA5.setReplaceB('x');
		tfCEA5.setCharGama('y');
		tfCEA5.setCharDelta('u');
		tfCEA5.setCharE('w');
		tfCEA5.run();
	}
}

void TFunctionOv::run()
{
	TFunctionEM tfEM;
	TFunctionAnf tfAnf;
	tfEM.setFunc(&tfAnf);
	tfEM.run();
}
