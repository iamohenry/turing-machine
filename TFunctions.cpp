#include "TFunctions.h"
#include <windows.h>

char TFunctions::m_cells[maxCellsSize] = { 0 };
int TFunctions::m_index = 0;
int TFunctions::m_prevIndex=0;
TFunctions TFunctions::instance;
int TFunctions::m_runDelay = 500;
bool TFunctions::m_isTestBreak = true;


TFunctions::TFunctions()	:
	tf_func(nullptr),
	tf_funb(nullptr),
	tf_fund(nullptr),
	tf_fune(nullptr)
{
}


TFunctions::~TFunctions()
{
}

void TFunctions::setFunc(void * func)
{
	tf_func = func;
}

void TFunctions::setFunb(void * funb)
{
	tf_funb = funb;
}

void TFunctions::setFund(void * fund)
{
	tf_fund = fund;
}

void TFunctions::setFune(void * fune)
{
	tf_fune = fune;
}

void TFunctions::setSearchA(char search)
{
	tf_searchA = search;
}

void TFunctions::setReplaceB(char replace)
{
	tf_replaceB = replace;
}

void TFunctions::setCharGama(char gama)
{
	tf_char_gama = gama;
}

void TFunctions::setCharDelta(char delta)
{
	tf_char_delta = delta;
}

void TFunctions::setCharE(char e)
{
	tf_char_e = e;
}

void TFunctions::reset()
{
	resetIndex();
	resetCells();
}

void TFunctions::resetCells()
{
	for (int i = 0; i < maxCellsSize; i++) {
		m_cells[i] = 0;
	}
}

void TFunctions::resetIndex()
{
	m_index = 120;
	m_prevIndex = 120;
}

void TFunctions::E()
{
	m_cells[m_index] = 0;
}

void TFunctions::R()
{
	m_index++;
}

void TFunctions::L()
{
	m_index--;
}

bool TFunctions::matchSth(char a)
{
	return (a==m_cells[m_index]);
}

void TFunctions::prSth(char a)
{
	if (0xFF == a)
		return;
	m_cells[m_index] = a;
}

void TFunctions::prepareTestSample(const char*  sampleStr)
{
	if (nullptr == sampleStr)
		return;
	char buffer[256] = { 0 };
	strcpy(buffer, sampleStr);
	char* ptr = buffer;
	int len = strlen(buffer);
	int i = 0;
	while (*ptr) {
		m_cells[m_index - len + i++] = *ptr++;
	}
}

void TFunctions::prepareUMTestSample(const char * sampleStr)
{
	if (nullptr == sampleStr)
		return;
	char buffer[256] = { 0 };
	strcpy(buffer, sampleStr);
	char* ptr = buffer;
	int len = strlen(buffer);
	int i = 0;
	m_cells[m_index - len - 4] = 'e';
	m_cells[m_index - len - 3] = 'e';
	m_cells[m_index - len - 2] = ';';
	while (*ptr) {
		m_cells[m_index - len + i] = *ptr++;
		i += 2;
	}
}

void TFunctions::prepareTestSample1()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
}

void TFunctions::prepareTestSample2()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[118] = 'a';
}

void TFunctions::prepareTestSample3()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = 'a';
}

void TFunctions::sendUpdateSignal()
{
	instance.updateSignal();
	Sleep(m_runDelay);
}

void TFunctions::runCycle(int freq) 
{
	if (freq < 0 || freq>90)
		return;
	m_runDelay = (100 - freq) * 10;
}

TFunctionFoo::TFunctionFoo()
{
}

TFunctionFoo::~TFunctionFoo()
{
}

void TFunctionFoo::run()
{
	qDebug() << "Func: " << m_funcName << " is running...";
}

void TFunctionFoo::setFuncName(QString funcName)
{
	m_funcName = funcName;
}
