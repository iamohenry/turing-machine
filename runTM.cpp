#include "runTM.h"
#include <thread>
#include <QCoreApplication.h>
#include "mainwindow.h"
#include "TFunctionF.h"
#include "TFunctionE.h"
#include "TFunctionEA.h"
#include "TFunctionPE.h"
#include "Windows.h"
#include "TFunctionG0101.h"
#include "TFunctionTD.h"
#include "TFunctionAddOne.h"
#include "TFunctionSqrtOfTwo.h"
#include "TFunctionC.h"
#include "TFunctionCE.h"
#include "TFunctionRE.h"
#include "TFunctionCR.h"
#include "TFunctionCP.h"
#include "TFunctionCPE.h"
#include "TFunctionG.h"
#include "TFunctionCon.h"
#include "TFunctionB.h"
#include "TFunctionSqrtOfThree.h"
#include "TFunctionSqrtOfFive.h"
using namespace std;

runTM::runTM()
{
	connect(TFunctions::getInstance(), 
		&TFunctions::updateSignal,
		MainWindow::getInstance(),
		&MainWindow::update);
}


runTM::~runTM()
{
	disconnect(TFunctions::getInstance(),
		&TFunctions::updateSignal,
		MainWindow::getInstance(),
		&MainWindow::update);
}

void runTM::testTFF()
{
	TFunctionFoo func;
	func.setFuncName("func");
	TFunctionFoo funb;
	funb.setFuncName("funb");

	TFunctionF tfF;
	tfF.setFunc(&func);
	tfF.setFunb(&funb);
	tfF.setSearchA('a');
	tfF.reset();
	tfF.prepareTestSample1();
	tfF.run();
	Sleep(2);
	qDebug() << "start test 2";
	tfF.reset();
	tfF.prepareTestSample2();
	tfF.run();
}

void runTM::testTFE()
{
	TFunctionFoo func;
	func.setFuncName("func");
	TFunctionFoo funb;
	funb.setFuncName("funb");

	TFunctionE tfE;
	tfE.setFunc(&func);
	tfE.setFunb(&funb);
	tfE.setSearchA('a');
	tfE.reset();
	tfE.prepareTestSample1();
	tfE.run();
	Sleep(2);
	qDebug() << "start test 2";
	tfE.reset();
	tfE.prepareTestSample2();
	tfE.run();
}

void runTM::testTFEA()
{
	TFunctionFoo funb;
	funb.setFuncName("funb");
	TFunctionEA tfEA;
	tfEA.setFunb(&funb);

	tfEA.setSearchA('a');
	tfEA.reset();
	qDebug() << "start test 1...";
	tfEA.prepareTestSample1();
	tfEA.run();
	Sleep(2);

	qDebug() << "start test 2...";
	tfEA.reset();
	tfEA.prepareTestSample2();
	tfEA.run();
	Sleep(2);

	qDebug() << "start test 3...";
	tfEA.reset();
	tfEA.prepareTestSample3();
	tfEA.run();
	Sleep(2);

	qDebug() << "start test 4...";
	tfEA.reset();
	tfEA.prepareTestSample4();
	tfEA.run();
	Sleep(2);

	qDebug() << "start test 5...";
	tfEA.reset();
	tfEA.prepareTestSample5();
	tfEA.run();
	Sleep(2);
}

void runTM::testTFPE()
{
	TFunctionFoo func;
	func.setFuncName("func");
	TFunctionPE tfPE;
	tfPE.setFunc(&func);
	tfPE.setReplaceB('b');

	tfPE.reset();
	qDebug() << "start test 1...";
	tfPE.prepareTestSample1();
	tfPE.run();
	Sleep(2);

	qDebug() << "start test 2...";
	tfPE.reset();
	tfPE.prepareTestSample2();
	tfPE.run();
	Sleep(2);
}

void runTM::testG0101()
{
#if 1
	TFunctionG0101 tf;
	tf.run();
#else
	TFunctionTD tf;
	tf.run();
#endif

}

void runTM::testTD()
{
	TFunctionTD tf;
	tf.run();
}

void runTM::testAddOne()
{
	TFunctionAddOne tf;
	tf.run();
}

void runTM::testSqrtOfTwo()
{
	TFunctionSqrtOfTwo tf;
	tf.run();
}

void runTM::testTFFL()
{
	TFunctionFoo func;
	func.setFuncName("func");
	TFunctionFoo funb;
	funb.setFuncName("funb");

	TFunctionFL tfFL;
	tfFL.setFunc(&func);
	tfFL.setFunb(&funb);
	tfFL.setSearchA('a');
	tfFL.reset();
	tfFL.prepareTestSample1();
	tfFL.run();
	Sleep(2);
	qDebug() << "start test 2";
	tfFL.setFunc(&func);
	tfFL.setFunb(&funb);
	tfFL.setSearchA('a');
	tfFL.reset();
	tfFL.prepareTestSample2();
	tfFL.run();

	Sleep(2);
	qDebug() << "start test 3";
	tfFL.setFunc(&func);
	tfFL.setFunb(&funb);
	tfFL.setSearchA('a');
	tfFL.reset();
	tfFL.prepareTestSample3();
	tfFL.run();
}

void runTM::testTFFR()
{
	TFunctionFoo func;
	func.setFuncName("func");
	TFunctionFoo funb;
	funb.setFuncName("funb");

	TFunctionFR tfFR;
	tfFR.setFunc(&func);
	tfFR.setFunb(&funb);
	tfFR.setSearchA('a');
	tfFR.reset();
	tfFR.prepareTestSample1();
	tfFR.run();
	Sleep(2);
	qDebug() << "start test 2";
	tfFR.setFunc(&func);
	tfFR.setFunb(&funb);
	tfFR.setSearchA('a');
	tfFR.reset();
	tfFR.prepareTestSample2();
	tfFR.run();

	Sleep(2);
	qDebug() << "start test 3";
	tfFR.setFunc(&func);
	tfFR.setFunb(&funb);
	tfFR.setSearchA('a');
	tfFR.reset();
	tfFR.prepareTestSample3();
	tfFR.run();
}

void runTM::testTFC()
{
	TFunctionFoo func;
	func.setFuncName("func");
	TFunctionFoo funb;
	funb.setFuncName("funb");
	TFunctionC tfC;
	tfC.setFunc(&func);
	tfC.setFunb(&funb);
	tfC.setSearchA('a');

	tfC.reset();
	tfC.prepareTestSample1();
	tfC.run();

	Sleep(2);
	qDebug() << "start test 2";

	tfC.setFunc(&func);
	tfC.setFunb(&funb);
	tfC.setSearchA('a');

	tfC.reset();
	tfC.prepareTestSample2();
	tfC.run();

	Sleep(2);
	qDebug() << "start test 3";

	tfC.setFunc(&func);
	tfC.setFunb(&funb);
	tfC.setSearchA('a');

	tfC.reset();
	tfC.prepareTestSample3();
	tfC.run();

	Sleep(2);
	qDebug() << "start test 4";

	tfC.setFunc(&func);
	tfC.setFunb(&funb);
	tfC.setSearchA('a');

	tfC.reset();
	tfC.prepareTestSample4();
	tfC.run();
}

void runTM::testTFCE()
{
	TFunctionFoo func;
	func.setFuncName("func");
	TFunctionFoo funb;
	funb.setFuncName("funb");

	TFunctionCE tfCE;
	tfCE.setFunc(&func);
	tfCE.setFunb(&funb);
	tfCE.setSearchA('a');

	tfCE.reset();
	tfCE.prepareTestSample1();
	tfCE.run();
}

void runTM::testTFCEA()
{
	TFunctionFoo funb;
	funb.setFuncName("funb");

	TFunctionCEA tfCEA;
	tfCEA.setFunb(&funb);
	tfCEA.setSearchA('a');

	tfCEA.reset();
	tfCEA.prepareTestSample1();
	tfCEA.run();
}

void runTM::testTFRE()
{
	TFunctionFoo func;
	func.setFuncName("func");
	TFunctionFoo funb;
	funb.setFuncName("funb");
	TFunctionRE tfRE;
	tfRE.setFunc(&func);
	tfRE.setFunb(&funb);
	tfRE.setSearchA('a');
	tfRE.setReplaceB('b');

	tfRE.reset();
	tfRE.prepareTestSample1();
	tfRE.run();
}

void runTM::testTFREA()
{
	TFunctionFoo funb;
	funb.setFuncName("funb");
	TFunctionREA tfREA;
	tfREA.setFunb(&funb);
	tfREA.setSearchA('a');
	tfREA.setReplaceB('b');

	tfREA.reset();
	tfREA.prepareTestSample1();
	tfREA.run();
}

void runTM::testTFCR()
{
	TFunctionFoo func;
	func.setFuncName("func");
	TFunctionFoo funb;
	funb.setFuncName("funb");

	TFunctionCR tfCR;
	tfCR.setFunc(&func);
	tfCR.setFunb(&funb);
	tfCR.setSearchA('a');

	tfCR.reset();
	tfCR.prepareTestSample1();
	tfCR.run();
}

void runTM::testTFCRA()
{
	TFunctionFoo funb;
	funb.setFuncName("funb");

	TFunctionCRA tfCRA;
	tfCRA.setFunb(&funb);
	tfCRA.setSearchA('a');

	tfCRA.reset();
	tfCRA.prepareTestSample1();
	tfCRA.run();
}

void runTM::testASCII()
{
	TFunctionFoo funb;
	funb.setFuncName("funb");
	funb.reset();
	for (unsigned char i = 0; i < 255; i++) {
		funb.prSth(i);
		funb.sendUpdateSignal();
		funb.R();
		funb.sendUpdateSignal();
	}
}

void runTM::testTFCP()
{
	TFunctionFoo func;
	func.setFuncName("func");
	TFunctionFoo funu;
	funu.setFuncName("funu");
	TFunctionFoo funk;
	funk.setFuncName("funk");

	TFunctionCP tfCP;
	tfCP.setFunc(&func);
	tfCP.setFunb(&funu);
	tfCP.setFund(&funk);

	tfCP.setSearchA('a');
	tfCP.setReplaceB('b');

	tfCP.reset();
	tfCP.prepareTestSample1();
	tfCP.run();

	
	Sleep(2);
	qDebug() << "start test 2...";

	tfCP.setFunc(&func);
	tfCP.setFunb(&funu);
	tfCP.setFund(&funk);

	tfCP.setSearchA('a');
	tfCP.setReplaceB('b');
	tfCP.reset();
	tfCP.prepareTestSample2();
	tfCP.run();

	Sleep(2);
	qDebug() << "start test 3...";

	tfCP.setFunc(&func);
	tfCP.setFunb(&funu);
	tfCP.setFund(&funk);

	tfCP.setSearchA('a');
	tfCP.setReplaceB('b');
	tfCP.reset();
	tfCP.prepareTestSample3();
	tfCP.run();

	Sleep(2);
	qDebug() << "start test 4...";

	tfCP.setFunc(&func);
	tfCP.setFunb(&funu);
	tfCP.setFund(&funk);

	tfCP.setSearchA('a');
	tfCP.setReplaceB('b');
	tfCP.reset();
	tfCP.prepareTestSample4();
	tfCP.run();
}

void runTM::testTFCPE()
{
	TFunctionFoo func;
	func.setFuncName("func");
	TFunctionFoo funu;
	funu.setFuncName("funu");
	TFunctionFoo funk;
	funk.setFuncName("funk");

	TFunctionCPE tfCPE;
	tfCPE.init(&func, &funu, &funk, 'a', 'b');
	tfCPE.test(1);

	qDebug() << "start test 2...";
	tfCPE.init(&func, &funu, &funk, 'a', 'b');
	tfCPE.test(2);

	qDebug() << "start test 3...";
	tfCPE.init(&func, &funu, &funk, 'a', 'b');
	tfCPE.test(3);

	qDebug() << "start test4...";
	tfCPE.init(&func, &funu, &funk, 'a', 'b');
	tfCPE.test(4);
}

void runTM::testTFCPEA()
{
	TFunctionFoo funu;
	funu.setFuncName("funu");
	TFunctionFoo funk;
	funk.setFuncName("funk");

	TFunctionCPEA tfCPEA;
	tfCPEA.init(&funu, &funk, 'a', 'b');
	tfCPEA.test(1);

	qDebug() << "start test 2...";
	tfCPEA.init(&funu, &funk, 'a', 'b');
	tfCPEA.test(2);

	qDebug() << "start test 3...";
	tfCPEA.init(&funu, &funk, 'a', 'b');
	tfCPEA.test(3);

	qDebug() << "start test4...";
	tfCPEA.init(&funu, &funk, 'a', 'b');
	tfCPEA.test(4);
}

void runTM::testTFG()
{
	TFunctionFoo func;
	func.setFuncName("func");

	TFunctionG tfG;
	tfG.setFunc(&func);

	tfG.test(1);

	qDebug() << "start test 2...";	
	tfG.test(2);

	qDebug() << "start test 3...";	
	tfG.test(3);

	qDebug() << "start test4...";	
	tfG.test(4);
}

void runTM::testTFGII()
{
	TFunctionFoo func;
	func.setFuncName("func");

	TFunctionGII tfGII;
	tfGII.setFunc(&func);
	tfGII.setSearchA('a');

	tfGII.test(1);

	qDebug() << "start test 2...";
	tfGII.test(2);

	qDebug() << "start test 3...";
	tfGII.test(3);

	qDebug() << "start test4...";
	tfGII.test(4);
}

void runTM::testTFPE2()
{
	TFunctionFoo func;
	func.setFuncName("func");

	TFunctionPE2 tfpe2;
	tfpe2.setFunc(&func);
	tfpe2.setSearchA('a');
	tfpe2.setReplaceB('b');

	for (int i = 1; i < 4; i++) {
		qDebug() << "start test %d...",i;
		tfpe2.test(i);
	}
}

void runTM::testTFCEA2()
{
	TFunctionFoo funb;
	funb.setFuncName("funb");
	TFunctionCEA2 tfcea2;
	tfcea2.setFunb(&funb);
	tfcea2.setSearchA('a');
	tfcea2.setReplaceB('b');

	for (int i = 1; i <=4; i++) {
		qDebug() << "start test %d...", i;
		tfcea2.test(i);
	}
}

void runTM::testTFCEA3()
{
		TFunctionFoo funb;
		funb.setFuncName("funb");
		TFunctionCEA3 tfcea3;
		tfcea3.setFunb(&funb);
		tfcea3.setSearchA('a');
		tfcea3.setReplaceB('b');
		tfcea3.setCharGama('r');

		for (int i = 1; i <= 4; i++) {
			qDebug() << "start test %d...", i;
			tfcea3.test(i);
#if 1
			TFunctions::m_isTestBreak = true;
			while (TFunctions::m_isTestBreak) {
				Sleep(1);
				QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
			}
#endif
		}
}

void runTM::testTFEM()
{
	TFunctionFoo func;
	func.setFuncName("func");
	TFunctionEM tfEM;
	tfEM.setFunc(&func);

	for (int i = 0; i < 4; i++) {
		qDebug() << "start test %d...", i+1;
		tfEM.test(i+1);
	}	
}

void runTM::testTFCon()
{
	TFunctionFoo func;
	func.setFuncName("func");

	TFunctionCon tfCon;
	tfCon.setFunc(&func);
	tfCon.setSearchA('a');

	for (int i = 0; i < 3; i++) {
		qDebug("start test %d...", i + 1);
		tfCon.test(i + 1);
		TFunctions::m_isTestBreak = true;
		while (TFunctions::m_isTestBreak) {
			Sleep(1);
			QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
		}
	}
}

void runTM::testTFB()
{
	TFunctionB tfB;	
	tfB.test();
}

void runTM::testTFSqrt3()
{
	TFunctionSqrtOfThree tf;
	tf.run();
}

void runTM::testTFSqrt5()
{
	TFunctionSqrtOfFive tf;
	tf.run();
}
