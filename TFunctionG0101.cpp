#include "TFunctionG0101.h"
#include <qdebug.h>



TFunctionG0101::TFunctionG0101()
{
}


TFunctionG0101::~TFunctionG0101()
{
}

void TFunctionG0101::run()
{
	qDebug() << "func G0101 is starting... ";
	reset();
	do {
		if (m_cells[m_index] == 0) {
			m_cells[m_index] = '0';
			sendUpdateSignal();			
		}
		else if (m_cells[m_index] == '0') {
			m_index++;
			sendUpdateSignal();
			m_index++;
			sendUpdateSignal();
			m_cells[m_index] = '1';
			sendUpdateSignal();
		}
		else if (m_cells[m_index] == '1') {
			m_index++;
			sendUpdateSignal();
			m_index++;
			sendUpdateSignal();
			m_cells[m_index] = '0';
			sendUpdateSignal();
		}
	} while (m_index < 200);
}
