#include "TFunctionEA.h"
#include "TFunctionE.h"



TFunctionEA::TFunctionEA()
{
}


TFunctionEA::~TFunctionEA()
{
}

void TFunctionEA::run()
{
	qDebug() << "func EA (erase all) is starting... ";
	TFunctionE tfE;
	tfE.setFunc(this);
	tfE.setFunb(tf_funb);
	tfE.setSearchA(tf_searchA);
	tfE.run();
}

void TFunctionEA::prepareTestSample1()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
}

void TFunctionEA::prepareTestSample2()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[118] = 'a';
}

void TFunctionEA::prepareTestSample3()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[118] = 'a';
	m_cells[128] = 'a';
	m_cells[121] = 'a';
	m_cells[126] = 'a';
	m_index = 130;
}

void TFunctionEA::prepareTestSample4()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = 'a';
	m_cells[118] = 'a';
	m_cells[119] = 'a';
	m_cells[120] = 'a';
	m_index = 130;
}

void TFunctionEA::prepareTestSample5()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = 'a';
	m_cells[118] = 'x';
	m_cells[119] = 'a';
	m_cells[120] = 'x';
	m_cells[121] = 'a';
	m_cells[122] = 'x';
	m_cells[123] = 'a';
	m_cells[124] = 'x';
	m_cells[125] = 'a';
	m_cells[126] = 'x';
	m_cells[127] = 'a';
	m_cells[128] = 'x';
	m_cells[129] = 'a';
	m_index = 130;
}
