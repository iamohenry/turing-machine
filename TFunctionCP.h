#pragma once
#include "TFunctions.h"

class TFunctionCP1 :
	public TFunctions
{
public:
	TFunctionCP1() {}
	~TFunctionCP1() {}
	void run();
};

class TFunctionCP2 :
	public TFunctions
{
public:
	TFunctionCP2() {}
	~TFunctionCP2() {}
	void run();
};

class TFunctionCP :
	public TFunctions
{
public:
	TFunctionCP();
	~TFunctionCP();
	void prepareTestSample1();
	void prepareTestSample2();
	void prepareTestSample3();
	void prepareTestSample4();
	void run();
};

