#pragma once
#include "TFunctions.h"

class TFunctionE1 :
	public TFunctions
{
public:
	TFunctionE1();
	~TFunctionE1();
	void run();
};


class TFunctionE :
	public TFunctions
{
public:
	TFunctionE();
	~TFunctionE();
	void run();
	void prepareTestSample1();
	void prepareTestSample2();
public:
	TFunctionE1 m_e1;
};

class TFunctionEM1 :
	public TFunctions
{
public:
	TFunctionEM1() {}
	~TFunctionEM1() {}
	void run();
	//void test(int order);
};

class TFunctionEM :
	public TFunctions
{
public:
	TFunctionEM() {}
	~TFunctionEM() {}
	void run();
	void test(int order);
	void prepareTestSample1();
	void prepareTestSample2();
};


