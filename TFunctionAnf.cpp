#include "TFunctionAnf.h"
#include "TFunctionG.h"
#include "TFunctionCon.h"
#include "TFunctionKom.h"


TFunctionAnf::TFunctionAnf()
{
}


TFunctionAnf::~TFunctionAnf()
{
}

void TFunctionAnf::run()
{
	TFunctionAnf1 tfAnf1;

	TFunctionGII tfG;
	tfG.setFunc(&tfAnf1);
	tfG.setSearchA(':');
	tfG.run();	
}

void TFunctionAnf1::run()
{
	TFunctionKom tfKom;

	TFunctionCon tfCon;
	tfCon.setFunc(&tfKom);
	tfCon.setSearchA('y');

	tfCon.run();
}
