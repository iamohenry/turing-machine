#include "TFunctionCR.h"
#include "TFunctionRE.h"
#include "TFunctionC.h"


TFunctionCR::TFunctionCR()
{
}


TFunctionCR::~TFunctionCR()
{
}

void TFunctionCR::prepareTestSample1()
{
	TFunctionRE tfRE;
	tfRE.prepareTestSample1();
}

void TFunctionCR::run()
{
	TFunctionRE tfRE;
	tfRE.setFunc(tf_func);
	tfRE.setFunb(tf_funb);
	tfRE.setSearchA(tf_searchA);
	tfRE.setReplaceB(tf_searchA);

	TFunctionC tfC;
	tfC.setFunc(&tfRE);
	tfC.setFunb(tf_funb);
	tfC.setSearchA(tf_searchA);
	tfC.run();
}

void TFunctionCRA::prepareTestSample1()
{
	TFunctionRE tfRE;
	tfRE.prepareTestSample1();
}

void TFunctionCRA::run()
{
#if 0
	TFunctionREA tfREA_B;
	tfREA_B.setFunb(tf_funb);
	tfREA_B.setSearchA('z');
	tfREA_B.setReplaceB(tf_searchA);


	TFunctionREA tfREA;
	tfREA.setFunb(&tfREA_B);
	tfREA.setSearchA(tf_searchA);
	tfREA.setReplaceB('z');
#else
	TFunctionREA tfREA;
	tfREA.setFunb(tf_funb);
	//tfREA.setSearchA(tf_searchA);ss
	tfREA.setSearchA('_');
	tfREA.setReplaceB(tf_searchA);
#endif

#if 1
	TFunctionCRT tfCRT;
	tfCRT.setFunc(this);
	tfCRT.setFunb(&tfREA);
	tfCRT.setSearchA(tf_searchA);
	tfCRT.run();
#else
	TFunctionCR tfCR;
	tfCR.setFunc(this);
	tfCR.setFunb(&tfREA);
	tfCR.setSearchA(tf_searchA);
	tfCR.run();
#endif
}

void TFunctionCRT::prepareTestSample1()
{
}

void TFunctionCRT::run()
{
	TFunctionRE tfRE;
	tfRE.setFunc(tf_func);
	tfRE.setFunb(tf_funb);
	tfRE.setSearchA(tf_searchA);
	tfRE.setReplaceB('_');

	TFunctionC tfC;
	tfC.setFunc(&tfRE);
	tfC.setFunb(tf_funb);
	tfC.setSearchA(tf_searchA);
	tfC.run();
}
