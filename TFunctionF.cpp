#include "TFunctionF.h"
#include <time.h>
#include <Windows.h>
#include <qdebug.h>

TFunctionL::TFunctionL()
{
}

TFunctionL::~TFunctionL()
{
}

void TFunctionL::run()
{
	L();
	sendUpdateSignal();
	TFunctions* tf = static_cast<TFunctions*> (tf_func);
	if (tf) {
		qDebug() << " func L(C) turn to func C";
		tf->run();
	}
}

void TFunctionR::run()
{
	R();
	sendUpdateSignal();
	TFunctions* tf = static_cast<TFunctions*> (tf_func);
	if (tf) {
		qDebug() << " func R(C) turn to func C";
		tf->run();
	}
}

TFunctionF::TFunctionF()
{
}


TFunctionF::~TFunctionF()
{
}

void TFunctionF::prepareTestSample1()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';	
}

void TFunctionF::prepareTestSample2()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[118] = 'a';
}

void TFunctionF::prepareTestSample3()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = 'a';
}

void TFunctionF::run()
{
	qDebug() << " func F is starting...";
	while ('e' != m_cells[m_index]) {
		m_index--;		//
		if (m_index < 0) {
			qDebug() << "index out of range.";
			return;
		}
		sendUpdateSignal();
		//Sleep(500);
	}
	if ('e' == m_cells[m_index]) {
		m_index--;		//
		if (m_index < 0) {
			qDebug() << "index out of range.";
			return;
		}
		sendUpdateSignal();
		//Sleep(500);
		qDebug() << " func F turn to F1";
		f1(tf_func, tf_funb, tf_searchA);
	}
}

void TFunctionF::f1(void * func, void * funb, char searchA)
{
	while ((searchA != m_cells[m_index]) && ('\0' != m_cells[m_index])) {
		m_index++;		//
		if (m_index >= maxCellsSize - 1) {
			qDebug() << "index out of range.";
			return;
		}
		sendUpdateSignal();
		//Sleep(500);
	}
	if (searchA==m_cells[m_index] ) {		
		TFunctions* tf = static_cast<TFunctions*> (func);
		if (tf) {
			qDebug() << " func F1 turn to func C";
			tf->run();
		}
	}else if ('\0' == m_cells[m_index]) {
		m_index++;		//
		if (m_index >= maxCellsSize - 1) {
			qDebug() << "index out of range.";
			return;
		}
		sendUpdateSignal();
		//Sleep(500);
		qDebug() << " func F1 turn to func F2";
		f2(func, funb, searchA);
	}
}

void TFunctionF::f2(void * func, void * funb, char searchA)
{
	if (searchA == m_cells[m_index]) {
		TFunctions* tf = static_cast<TFunctions*> (func);		
		if (tf) {
			qDebug() << " func F1 turn to func C";
			tf->run();
		}
	}else if ((searchA != m_cells[m_index]) && ('\0' != m_cells[m_index])) {
		m_index++;		//
		if (m_index >= maxCellsSize - 1) {
			qDebug() << "index out of range.";
			return;
		}
		sendUpdateSignal();
		//Sleep(500);
		qDebug() << " func F2 turn to func F1";
		f1(func, funb, searchA);
	}else if ('\0' == m_cells[m_index]) {
		m_index++;		//
		if (m_index >= maxCellsSize - 1) {
			qDebug() << "index out of range.";
			return;
		}
		sendUpdateSignal();
		//Sleep(500);
		TFunctions* tf = static_cast<TFunctions*> (funb);
		if (tf) {
			qDebug() << " func F2 turn to func B";
			tf->run();
		}
	}
}

TFunctionFR::TFunctionFR()
	:TFunctionF()
{
}

TFunctionFR::~TFunctionFR()
{	
}

void TFunctionFR::run()
{
	TFunctions* tf = static_cast<TFunctions*> (tf_func);
	TFunctionR tf_r;
	if (tf) {
		tf_r.setFunc(tf);
	}
	setFunc(&tf_r);
	TFunctionF::run();
}

TFunctionFL::TFunctionFL()
	:TFunctionF()
{
}

TFunctionFL::~TFunctionFL()
{
}

void TFunctionFL::run()
{
	TFunctions* tf = static_cast<TFunctions*> (tf_func);
	TFunctionL tf_l;
	if (tf) {
		tf_l.setFunc(tf);
	}
	setFunc(&tf_l);
	TFunctionF::run();
}
