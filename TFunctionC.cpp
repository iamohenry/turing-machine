#include "TFunctionC.h"


void TFunctionC1::run()
{
	TFunctionPE tfPE;
	tfPE.setFunc(tf_func);
	tfPE.setReplaceB(m_cells[m_index]);
	tfPE.run();
}


TFunctionC::TFunctionC()
{

}


TFunctionC::~TFunctionC()
{
}



void TFunctionC::run()
{
	TFunctionC1 tfC1;
	tfC1.setFunc(tf_func);
	TFunctionFL tfFL;
	tfFL.setFunc(&tfC1);
	tfFL.setFunb(tf_funb);
	tfFL.setSearchA(tf_searchA);
	tfFL.run();
}

void TFunctionC::prepareTestSample4()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'b';
	m_cells[119] = '0';
	m_cells[120] = 'a';
}


