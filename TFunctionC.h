#pragma once
#include "TFunctions.h"
#include "TFunctionF.h"
#include "TFunctionPE.h"

class TFunctionC1 :
	public TFunctions
{
public:
	TFunctionC1() {}
	~TFunctionC1() {}
	void run();
};

class TFunctionC :
	public TFunctions
{
public:
	TFunctionC();
	~TFunctionC();
	void run();
	void prepareTestSample4();
};

