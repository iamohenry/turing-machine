#pragma once
#include "TFunctions.h"
class TFunctionCPE :
	public TFunctions
{
public:
	TFunctionCPE();
	~TFunctionCPE();
	void init(TFunctions* func, 
		TFunctions* funb, 
		TFunctions* fund, 
		char alpha, 
		char beta);
	void test(int order);
	void prepareTestSample1();
	void prepareTestSample2();
	void prepareTestSample3();
	void prepareTestSample4();
	void run();
};

class TFunctionCPEA :
	public TFunctions
{
public:
	TFunctionCPEA() {}
	~TFunctionCPEA() {}
	void init(TFunctions* func,
		TFunctions* funb,
		char alpha,
		char beta);
	void test(int order);
	void prepareTestSample1();
	void prepareTestSample2();
	void prepareTestSample3();
	void prepareTestSample4();
	void run();
};

