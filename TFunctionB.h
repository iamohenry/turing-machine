#pragma once
#include "TFunctions.h"

class TFunctionB1 :
	public TFunctions
{
public:
	TFunctionB1(){}
	~TFunctionB1(){}
	void run();
};

class TFunctionB :
	public TFunctions
{
public:
	TFunctionB();
	~TFunctionB();
	void run();
	void test();
};

