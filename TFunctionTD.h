#pragma once
#include "TFunctions.h"



class TFunctionTD :
	public TFunctions
{
public:
	TFunctionTD();
	~TFunctionTD();
	void run();
private:
	enum eConfig_TD {
		eConfig_TD_b,
		eConfig_TD_c,
		eConfig_TD_q,
		eConfig_TD_p,
		eConfig_TD_f,
	};
	eConfig_TD m_config;
};

