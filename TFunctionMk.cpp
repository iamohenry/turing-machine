#include "TFunctionMk.h"
#include "TFunctionG.h"
#include "TFunctionCon.h"
#include "TFunctionSh.h"
#include "TFunctionF.h"



TFunctionMk::TFunctionMk()
{
}


TFunctionMk::~TFunctionMk()
{
}

void TFunctionMk::run()
{
	TFunctionMk1 tfMK1;
	TFunctionGII tfGII;
	tfGII.setFunc(&tfMK1);
	tfGII.setSearchA(':');
	tfGII.run();
}

void TFunctionMk1::run()
{
	TFunctionMk2 tfMK2;
	while (!matchSth('A')) {
		R();
		sendUpdateSignal();
		R();
		sendUpdateSignal();
	}
	if (matchSth('A')) {
		for (int i = 0; i < 4; i++) {
			L();
			sendUpdateSignal();
		}
		tfMK2.run();
	}
}

void TFunctionMk2::run()
{
	TFunctionMk3 tfMk3;
	TFunctionMk4 tfMk4;
	while (matchSth('C')) {
		R();
		sendUpdateSignal();
		prSth('x');
		sendUpdateSignal();
		for (int i = 0; i < 3; i++) {
			L();
			sendUpdateSignal();
		}
	}
	if (matchSth(':')) {
		tfMk4.run();
	}
	else if (matchSth('D')) {
		R();
		sendUpdateSignal();
		prSth('x');
		sendUpdateSignal();
		for (int i = 0; i < 3; i++) {
			L();
			sendUpdateSignal();
		}
		tfMk3.run();
	}
}

void TFunctionMk3::run()
{
	TFunctionMk4 tfMk4;
	while (!matchSth(':')) {
		R();
		sendUpdateSignal();
		prSth('v');
		sendUpdateSignal();
		for (int i = 0; i < 3; i++) {
			L();
			sendUpdateSignal();
		}
	}
	if (matchSth(':')) {
		tfMk4.run();
	}
}

void TFunctionMk4::run()
{
	TFunctionCon tfCon;
	TFunctionL tfL_ext;
	TFunctionL tfL_inn;
	TFunctionMk5 tfMk5;
	tfL_inn.setFunc(&tfMk5);
	tfL_ext.setFunc(&tfL_inn);
	tfCon.setFunc(&tfL_ext);
	tfCon.setSearchA(0xFF);
	tfCon.run();
}

void TFunctionMk5::run()
{
	TFunctionSh tfSh;
	while (!matchSth(0)) {
		R();
		sendUpdateSignal();
		prSth('w');
		sendUpdateSignal();
		R();
		sendUpdateSignal();
	}
	if (matchSth(0)) {
		prSth(':');
		sendUpdateSignal();
		tfSh.run();
	}
}
