#include "TFunctionE.h"
#include "TFunctionF.h"

TFunctionE1::TFunctionE1()
{
}

TFunctionE1::~TFunctionE1()
{
}

void TFunctionE1::run()
{	
	qDebug() << "func E1 is starting... ";
	m_cells[m_index] = '\0';	
	sendUpdateSignal();
	TFunctionFoo *func = static_cast<TFunctionFoo*>( tf_func);
	if (func) {
		qDebug() << " func E1 turn to func C";
		func->run();
	}
}


TFunctionE::TFunctionE()
{
}


TFunctionE::~TFunctionE()
{
}

void TFunctionE::run()
{
	qDebug() << "func E is starting... ";
	TFunctionF tfF;	
	m_e1.setFunc(tf_func);
	m_e1.setFunb(tf_funb);
	m_e1.setSearchA(tf_searchA);
	tfF.setFunc(static_cast<void*>(&m_e1));
	tfF.setFunb(tf_funb);
	tfF.setSearchA(tf_searchA);
	tfF.run();
}

void TFunctionE::prepareTestSample1()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
}

void TFunctionE::prepareTestSample2()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[118] = 'a';
}

void TFunctionEM::run()
{
	TFunctionEM1 tfEM1;
	tfEM1.setFunc(tf_func);
	while (!matchSth('e')) {
		L();
		sendUpdateSignal();
	}
	if (matchSth('e')) {
		R();
		sendUpdateSignal();
		tfEM1.run();
	}
}

void TFunctionEM::test(int order)
{
	reset();
	switch (order)
	{
	case 1:
		prepareTestSample("ee1a0bxryazbxr");
		break;
	case 2:
		prepareTestSample1();
		break;
	case 3:
		prepareTestSample("eea1b0c1d0x1f0");
		break;
	case 4:
		prepareTestSample2();
		break;
	default:
		break;
	}
	run();
	sendUpdateSignal();
}

void TFunctionEM::prepareTestSample1()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[118] = 'a';
	m_cells[120] = 'b';
	m_cells[122] = 'c';
	m_cells[124] = 'd';
}

void TFunctionEM::prepareTestSample2()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = 'a';
	m_cells[118] = 'a';
	//m_cells[119] = 'b';
	m_cells[120] = 'a';
	m_cells[121] = 'c';
	m_cells[122] = 'a';
	m_cells[124] = 'd';
	m_cells[125] = 'd';
}

void TFunctionEM1::run()
{
	while (!matchSth(0)) {
		R();
		sendUpdateSignal();
		E();
		sendUpdateSignal();
		R();
		sendUpdateSignal();
	}
	if (matchSth(0)) {
		if (tf_func) {
			TFunctions* func = static_cast<TFunctions*>(tf_func);
			func->run();
		}
	}
}
