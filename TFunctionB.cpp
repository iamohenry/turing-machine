#include "TFunctionB.h"
#include "TFunctionAnf.h"
#include "TFunctionF.h"


TFunctionB::TFunctionB()
{
}


TFunctionB::~TFunctionB()
{
}

void TFunctionB::run()
{
	TFunctionF tfF;
	TFunctionB1 tfB1;
	tfF.setFunc(&tfB1);
	tfF.setFunb(&tfB1);
	
	tfF.setSearchA('>');  
	/* origin symbol is '::', 
	since there is no '::' in ascII code, 
	so replace '::' with '>', 
	means: program running.
	*/
	tfF.run();
}

void TFunctionB::test()
{
	reset();
	prepareUMTestSample("DADDCRDAA;DAADDCCRDA>");
	run();
}

void TFunctionB1::run()
{
	TFunctionAnf tfAnf;
	R();
	sendUpdateSignal();
	R();
	sendUpdateSignal();
	prSth(':');
	sendUpdateSignal();
	R();
	sendUpdateSignal();
	R();
	sendUpdateSignal();
	prSth('D');
	sendUpdateSignal();
	R();
	sendUpdateSignal();
	R();
	sendUpdateSignal();
	prSth('A');
	sendUpdateSignal();
	tfAnf.run();
}
