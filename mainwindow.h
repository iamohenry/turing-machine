﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QPushButton>
#include <QLabel>
#include <QKeyEvent>

namespace Ui {
class MainWindow;
}

const int maxBtnSize = 128;
class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
	void setCells(char* cell);
	void setIndex(int *index);
	static MainWindow* getInstance();
	static void setInstance(MainWindow* window);
signals:
	void tuneRunFreq(int value);
	void startTestCEA3();

private:
    void initBtns();
    void init();
    void initIndex();
    void updateBtns();
    void updateIndex();
	void updateRWIndex();
public slots:
    void update(void);  // update the ui widgets;
private:
    void clearBtns();
    void clearIndex();
	void clearRWIndex();
	void keyReleaseEvent(QKeyEvent * event);
	private slots:
	void on_pbContinue_pressed();
	void on_pbContinue_released();
	void on_pbRunF_released();
	void on_pbRunE_released();
	void on_pbRunG0101_released();
	void on_pbRunTD_released();
	void on_pbRunAddOne_released();
	void on_pbRunSqrtOfTwo_released();
	void on_pbRunEA_released();
	void on_pbRunPE_released();
	void on_pbRunFR_released();
	void on_pbRunFL_released();
	void on_pbRunC_released();
	void on_pbRunCE_released();
	void on_pbRunCEA_released();
	void on_pbRunRE_released();
	void on_pbRunREA_released();
	void on_pbRunCR_released();
	void on_pbRunCRA_released();
	void on_pbRunASCII_released();
	void on_pbRunCP_released();
	void on_pbRunCPE_released();
	void on_pbRunCPEA_released();
	void on_pbRunG_released();
	void on_pbRunGII_released();
	void on_pbRunPE2_released();
	void on_pbRunCEA2_released();
	void testCEA3();
	void on_pbRunCEA3_released();
	void on_hs_runfreq_valueChanged(int value);
	void on_pbRunEM_released();
	void on_pbRunCon_released();
	void on_pbRunB_released();
	void on_pbRunSqrtOfThree_released();
	void on_pbRunSqrtOfFive_released();

private:
    Ui::MainWindow *ui;
	char *m_cells; 
    int *m_curIndex;
	static uint m_prevIndex;
	static MainWindow* instance;
	QPushButton*  m_btns[maxBtnSize];
	QLabel* m_lbls[maxBtnSize];
	uint btnSize;
};

#endif // MAINWINDOW_H
