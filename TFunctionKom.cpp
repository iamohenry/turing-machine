#include "TFunctionKom.h"
#include "TFunctionKmp.h"
#include "TFunctionCon.h"



TFunctionKom::TFunctionKom()
{
}


TFunctionKom::~TFunctionKom()
{
}

void TFunctionKom::run()
{
	TFunctionKmp tfKmp;

	TFunctionCon tfCon;
	tfCon.setFunc(&tfKmp);
	tfCon.setSearchA('x');

	while (matchSth('z') 
		||		(!matchSth('z')
					&& !matchSth(';') )			
		) {
		if (matchSth('z')) {
			L();
			sendUpdateSignal();
			L();
			sendUpdateSignal();
		}
		else {
			L();
			sendUpdateSignal();
		}
	}

	if (matchSth(';')) {
		R();
		sendUpdateSignal();
		prSth('z');
		sendUpdateSignal();
		L();
		sendUpdateSignal();
		tfCon.run();
	}
}
