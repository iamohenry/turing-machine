#pragma once
#include "TFunctions.h"

class TFunctionSim1 :
	public TFunctions
{
public:
	TFunctionSim1() {}
	~TFunctionSim1() {}
	void run();
};

class TFunctionSim2 :
	public TFunctions
{
public:
	TFunctionSim2() {}
	~TFunctionSim2() {}
	void run();
};

class TFunctionSim3 :
	public TFunctions
{
public:
	TFunctionSim3(){}
	~TFunctionSim3() {}
	void run();
};

class TFunctionSim :
	public TFunctions
{
public:
	TFunctionSim();
	~TFunctionSim();
	void run();
};

