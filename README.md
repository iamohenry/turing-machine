# turing-machine

#### 介绍
build turing machine with Qt/C++. 
program could runs in vs2015+qt vs tools. 
Qt version Qt5.12.4;
currently windows os needed. but it's not neccessary.

这个软件是模拟了图灵在1936年发表的《论可计算数及其在判定性问题上的应用》论文里面，提到的图灵机和通用图灵机的功能；揭示了采用通用图灵机模拟任何一种图灵机都是可行的，从而证明了通用计算机器的存在，为电子计算机（computer，原意是为了完成某种数学问题而执行特定计算功能的人类计算者，现在已经演变成电脑的代名词）的诞生奠定了理论基础；

B站上有一门计算机入门课：从与非门到俄罗斯方块。里面提出一个问题，为什么家里有洗衣机，冰箱，空调，燃气灶...许许多多不一样的机器，而我们只需要一部手机或者一台电脑，通过app或应用程序，就可以实现阅读、看电影、听音乐这多种多样的功能？我们对这些事情习以为常，却没有去深究这背后隐藏的通用性的原理。而这种通用性，正是图灵通过这篇论文，带给我们的礼物。

#### 软件架构
软件架构说明

这个程序不是很复杂，有一个界面，用来模拟纸带和读写头；上面有很多按钮，这些按钮用来模拟图灵机和通用图灵机，比如，能够打印010101...序列的图灵机，能够打印0010110111...序列的图灵机，能够依次打印全体自然数的图灵机，以及能够计算 sqrt(2)这样的无限不循环小数序列的图灵机；并且测试了通用图灵机的骨架表（类似于函数库）中的所有函数；最后演示了通用图灵机是怎么模拟打印010101...序列的图灵机的，从而证实图灵所言不虚。

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
