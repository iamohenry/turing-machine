#include "TFunctionPE.h"
#include "TFunctionF.h"

TFunctionPE1::TFunctionPE1()
{
}

TFunctionPE1::~TFunctionPE1()
{
}

void TFunctionPE1::run()
{
	qDebug() << "func PE1 is starting... ";
	while (m_cells[m_index]) {
		R();
		sendUpdateSignal();
		R();
		sendUpdateSignal();
	}
	if (0 == m_cells[m_index]) {
		m_cells[m_index] = tf_replaceB;
		sendUpdateSignal();
		TFunctionFoo *func = static_cast<TFunctionFoo*>(tf_func);
		if (func) {
			qDebug() << " func PE1 turn to func C";
			func->run();
		}
	}
}

TFunctionPE::TFunctionPE()
{
	tf_searchA = 'e';
}


TFunctionPE::~TFunctionPE()
{
}

void TFunctionPE::run()
{
	qDebug() << "func PE is starting... ";
	TFunctionF tfF;
	m_pe1.setFunc(tf_func);
	//m_pe1.setFunb(tf_funb);
	//m_pe1.setSearchA(tf_searchA);
	m_pe1.setReplaceB(tf_replaceB);
	tfF.setFunc(static_cast<void*>(&m_pe1));
	tfF.setFunb(tf_func);
	tfF.setSearchA(tf_searchA);
	tfF.run();
}

void TFunctionPE::prepareTestSample1()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = 'a';
	m_cells[119] = 'a';
	m_cells[121] = 'a';
	m_cells[123] = 'a';
	m_cells[125] = 'a';
}

void TFunctionPE::prepareTestSample2()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[118] = 'a';
	m_cells[120] = 'a';
	m_cells[122] = 'a';
	m_cells[124] = 'a';
	m_cells[126] = 'a';
}

void TFunctionPE2::run()
{
	TFunctionPE tfPE_c;
	tfPE_c.setFunc(tf_func);
	tfPE_c.setReplaceB(tf_replaceB);  // in class TFunctionPE, the tf_searchA is fixed as 'a';
	TFunctionPE tfPE;
	tfPE.setFunc(&tfPE_c);
	tfPE.setReplaceB(tf_searchA);
	tfPE.run();
}

void TFunctionPE2::test(int order)
{
	reset();
	TFunctionPE tfpe;
	switch (order) {
	case 1:
		prepareTestSample("ee1a0b0a1b");
		break;
	case 2:
		tfpe.prepareTestSample1();
		break;
	case 3:
		tfpe.prepareTestSample2();
		break;
	case 4:
		tfpe.prepareTestSample3();
	default:
		break;
	}
	run();
	sendUpdateSignal();
}
