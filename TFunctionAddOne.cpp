#include "TFunctionAddOne.h"



TFunctionAddOne::TFunctionAddOne()
{
}


TFunctionAddOne::~TFunctionAddOne()
{
}

void TFunctionAddOne::run()
{
	qDebug() << "func add one is starting...";
	reset();
	m_config = eConfig_AddOne_Begin;
	do {
		switch (m_config) {
		case eConfig_AddOne_Begin: {
			if (m_cells[m_index] == 0) {
				m_cells[m_index] = '0';
				sendUpdateSignal();
				m_config = eConfig_AddOne_Increment;
			}
		}
			break;
		case eConfig_AddOne_Increment: {

			if (0 == m_cells[m_index]) {
				m_cells[m_index] = '1';
				sendUpdateSignal();
				m_config = eConfig_AddOne_rewind;
			}
			else if ('0' == m_cells[m_index]) {
				m_cells[m_index] = '1';
				sendUpdateSignal();
				m_config = eConfig_AddOne_rewind;
			}
			else if ('1' == m_cells[m_index]) {
				m_cells[m_index] = '0';
				sendUpdateSignal();
				m_index--;
				sendUpdateSignal();				
			}
		}
			break;
		case eConfig_AddOne_rewind: {
			if (0 == m_cells[m_index]) {
				m_index--;
				sendUpdateSignal();
				m_config = eConfig_AddOne_Increment;
			}
			else {
				m_index++;
				sendUpdateSignal();
			}
		}
			break;
		default:
			break;
		}
	} while (m_index > 50);
}
