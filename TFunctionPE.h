#pragma once
#include "TFunctions.h"

class TFunctionPE1 :
	public TFunctions
{
public:
	TFunctionPE1();
	~TFunctionPE1();
	void run();
};

class TFunctionPE :
	public TFunctions
{
public:
	TFunctionPE();
	~TFunctionPE();
	void run();
	void prepareTestSample1();
	void prepareTestSample2();
private:
	TFunctionPE1 m_pe1;
};

class TFunctionPE2 :
	public TFunctions
{
public:
	TFunctionPE2() {}
	~TFunctionPE2() {}
	void run();
	void test(int order);
};