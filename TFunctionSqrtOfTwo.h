#pragma once
#include "TFunctions.h"
class TFunctionSqrtOfTwo :
	public TFunctions
{
public:
	TFunctionSqrtOfTwo();
	~TFunctionSqrtOfTwo();
	void run();
private:
	double calResult();	
private:
	enum eConfig_SqrtOfTwo {
		eConfig_SqrtOfTwo_begin,
		eConfig_SqrtOfTwo_new,
		eConfig_SqrtOfTwo_mark_digits,
		eConfig_SqrtOfTwo_find_x,
		eConfig_SqrtOfTwo_first_r,
		eConfig_SqrtOfTwo_last_r,
		eConfig_SqrtOfTwo_find_digits,
		eConfig_SqrtOfTwo_find_1st_digit,
		eConfig_SqrtOfTwo_found_1st_digit,
		eConfig_SqrtOfTwo_find_2nd_digit,
		eConfig_SqrtOfTwo_found_2nd_digit,
		eConfig_SqrtOfTwo_add_zero,
		eConfig_SqrtOfTwo_add_one,
		eConfig_SqrtOfTwo_carry,
		eConfig_SqrtOfTwo_add_finished,
		eConfig_SqrtOfTwo_erase_old_x,
		eConfig_SqrtOfTwo_print_new_x,
		eConfig_SqrtOfTwo_erase_old_y,
		eConfig_SqrtOfTwo_print_new_y,
		eConfig_SqrtOfTwo_reset_new_x,
		eConfig_SqrtOfTwo_flag_result_digits,
		eConfig_SqrtOfTwo_unflag_result_digits,
		eConfig_SqrtOfTwo_new_digit_is_zero,
		eConfig_SqrtOfTwo_print_zero_digit,
		eConfig_SqrtOfTwo_new_digit_is_one,
		eConfig_SqrtOfTwo_print_one_digit,
		eConfig_SqrtOfTwo_cleanup,
	};
	eConfig_SqrtOfTwo m_config;
};

