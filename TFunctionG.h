#pragma once
#include "TFunctions.h"
class TFunctionG :
	public TFunctions
{
public:
	TFunctionG();
	~TFunctionG();
	void test(int order);
	void prepareTestSample2();
	void prepareTestSample3();
	void prepareTestSample4();
	void reset();
	void run();
	void g1();
};

class TFunctionGII :
	public TFunctions
{
public:
	TFunctionGII() {}
	~TFunctionGII() {}
	void test(int order);
	void run();
};

class TFunctionGII_1 :
	public TFunctions
{
public:
	TFunctionGII_1() {}
	~TFunctionGII_1() {}
	void run();
};
