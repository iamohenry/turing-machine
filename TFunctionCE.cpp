#include "TFunctionCE.h"
#include "TFunctionE.h"
#include "TFunctionC.h"
#include "Windows.h"
#include <thread>
using namespace std;


TFunctionCE::TFunctionCE()
{
}


TFunctionCE::~TFunctionCE()
{
}

void TFunctionCE::prepareTestSample1()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = '0';
	m_cells[120] = 'a';
	m_cells[121] = 'x';
	m_cells[122] = 'a';
	m_cells[123] = 'y';
	m_cells[124] = 'b';
}

void TFunctionCE::run()
{
	TFunctionE tfE;
	tfE.setFunc(tf_func);
	tfE.setFunb(tf_funb);
	tfE.setSearchA(tf_searchA);

	TFunctionC tfC;
	tfC.setFunc(&tfE);
	tfC.setFunb(tf_funb);
	tfC.setSearchA(tf_searchA);

	tfC.run();
}

void TFunctionCEA::prepareTestSample1()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = '0';
	m_cells[120] = 'a';
	m_cells[121] = 'x';
	m_cells[122] = 'a';
	m_cells[123] = 'y';
	m_cells[124] = 'b';
}

void TFunctionCEA::run()
{
	TFunctionCE tfCE;
	tfCE.setFunc(this);
	tfCE.setFunb(tf_funb);
	tfCE.setSearchA(tf_searchA);

	tfCE.run();
}

void TFunctionCEA2::prepareTestSample1()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = 'x';
	m_cells[121] = '0';
	m_cells[122] = 'a';
	m_cells[123] = 'x';
	m_cells[124] = 'a';
	m_cells[125] = 'y';
	m_cells[126] = 'b';
}

void TFunctionCEA2::run()
{
	TFunctionCEA tfCEA_b;
	tfCEA_b.setFunb(tf_funb);
	tfCEA_b.setSearchA(tf_replaceB);
	TFunctionCEA tfCEA;
	tfCEA.setFunb(&tfCEA_b);
	tfCEA.setSearchA(tf_searchA);
	tfCEA.run();
}

void TFunctionCEA2::test(int order)
{
	reset();
	switch (order) {
	case 1:
		prepareTestSample("ee1a0a0a1a1b0b1b0b");
		break;
	case 2:
		prepareTestSample("ee1a0b0a1b1a0b1a0b");
		break;
	case 3:
		prepareTestSample("ee1a0a0b1b1a0b1a0b");
		break;
	case 4:
		prepareTestSample1();
		break;
	default:
		break;
	}
	run();
	sendUpdateSignal();
	
}

void TFunctionCEA3::prepareTestSample1()  //1 y x 0 y
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = 'x';
	m_cells[121] = '0';
	m_cells[122] = 'r';
	m_cells[123] = 'x';
	m_cells[125] = 'y';
	m_cells[126] = 'r';
	m_cells[127] = 'x';
	m_cells[128] = 'b';
	m_cells[129] = 'y';
	m_cells[130] = 'a';
}

void TFunctionCEA3::run()
{
	TFunctionCEA2 tfCEA2_b;
	tfCEA2_b.setFunb(tf_funb);
	tfCEA2_b.setSearchA(tf_replaceB);
	tfCEA2_b.setReplaceB(tf_char_gama);

	TFunctionCEA tfCEA;
	tfCEA.setFunb(&tfCEA2_b);
	tfCEA.setSearchA(tf_searchA);
	tfCEA.run();
}

void TFunctionCEA3::test(int order)
{
		reset();
		switch (order) {
		case 1:
			prepareTestSample("ee");// 1a0b1r");
			//prepareTestSample("ee1a0a0a1a1b0b1b0b1r0r1r1r");  //1001 1010 1011			
			break;
		case 2:
			prepareTestSample("ee1r0r0r1r1b0b1b0b1a0a1a1a"); //1011 1010 1001
			break;
		case 3:
			prepareTestSample("ee1a0b0r1b1r0r1a0b1r0a1a1b");//1101 0101 0101
			break;
		case 4:
			prepareTestSample1(); //1 y x 0 y
			break;
		default:
			break;
		}
		run();
		sendUpdateSignal();
}

void TFunctionCEA4::prepareTestSample1()
{
}

void TFunctionCEA4::run()
{
	TFunctionCEA3 tfCEA3_b;
	tfCEA3_b.setFunb(tf_funb);
	tfCEA3_b.setSearchA(tf_replaceB);
	tfCEA3_b.setReplaceB(tf_char_gama);
	tfCEA3_b.setCharGama(tf_char_delta);

	TFunctionCEA tfCEA;
	tfCEA.setFunb(&tfCEA3_b);
	tfCEA.setSearchA(tf_searchA);
	tfCEA.run();
}

void TFunctionCEA4::test(int order)
{
}

void TFunctionCEA5::prepareTestSample1()
{
}

void TFunctionCEA5::run()
{
	TFunctionCEA4 tfCEA4_b;
	tfCEA4_b.setFunb(tf_funb);
	tfCEA4_b.setSearchA(tf_replaceB);
	tfCEA4_b.setReplaceB(tf_char_gama);
	tfCEA4_b.setCharGama(tf_char_delta);
	tfCEA4_b.setCharDelta(tf_char_e);

	TFunctionCEA tfCEA;
	tfCEA.setFunb(&tfCEA4_b);
	tfCEA.setSearchA(tf_searchA);
	tfCEA.run();
}

void TFunctionCEA5::test(int order)
{
}
