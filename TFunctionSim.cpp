#include "TFunctionSim.h"
#include "TFunctionCon.h"
#include "TFunctionEA.h"
#include "TFunctionMk.h"
#include "TFunctionF.h"


TFunctionSim::TFunctionSim()
{
}


TFunctionSim::~TFunctionSim()
{
}

void TFunctionSim::run()
{
	TFunctionSim1 tfSim1;
	TFunctionFL tfFL;
	tfFL.setFunc(&tfSim1);
	tfFL.setFunb(&tfSim1);
	tfFL.setSearchA('z');
	tfFL.run();
}

void TFunctionSim1::run()
{
	TFunctionSim2 tfSim2;
	TFunctionCon tfCon;
	tfCon.setFunc(&tfSim2);
	tfCon.setSearchA(0xFF);
	tfCon.run();
}

void TFunctionSim2::run()
{
	TFunctionSim3 tfSim3;
	while (!matchSth('A')) {
		L();
		sendUpdateSignal();
		prSth('u');
		sendUpdateSignal();
		R();
		sendUpdateSignal();
		R();
		sendUpdateSignal();
		R();
		sendUpdateSignal();
	}
	if (matchSth('A')) {
		tfSim3.run();
	}
}

void TFunctionSim3::run()
{
	TFunctionMk tfMk;
	TFunctionEA tfEA;

	tfEA.setFunb(&tfMk);
	tfEA.setSearchA('z');
	while (matchSth('A')) {
		L();
		sendUpdateSignal();
		prSth('y');
		sendUpdateSignal();
		R();
		sendUpdateSignal();
		R();
		sendUpdateSignal(); 
		R();
		sendUpdateSignal();
	}
	if (!matchSth('A')) {
		L();
		sendUpdateSignal();
		prSth('y');
		tfEA.run();
	}
}
