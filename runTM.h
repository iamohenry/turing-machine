#pragma once
#include "TFunctions.h"
#include <qobject.h>
class runTM:public QObject
{
	Q_OBJECT
public:
	runTM();
	~runTM();
	void testTFF();
	void testTFE();
	void testTFEA();
	void testTFPE();
	void testG0101();
	void testTD();
	void testAddOne();
	void testSqrtOfTwo();
	void testTFFL();
	void testTFFR();
	void testTFC();
	void testTFCE();
	void testTFCEA();
	void testTFRE();
	void testTFREA();
	void testTFCR();
	void testTFCRA();
	void testASCII();
	void testTFCP();
	void testTFCPE();
	void testTFCPEA();
	void testTFG();
	void testTFGII();
	void testTFPE2();

	void testTFCEA2();
	void testTFCEA3();
	void testTFEM();
	void testTFCon();
	void testTFB();
	void testTFSqrt3();
	void testTFSqrt5();

#if 0
private:
	TFunctions* m_tf;
#endif
};

