#pragma once
#include "TFunctions.h"
class TFunctionCR :
	public TFunctions
{
public:
	TFunctionCR();
	~TFunctionCR();
	void prepareTestSample1();
	void run();
};

class TFunctionCRT :
	public TFunctions
{
public:
	TFunctionCRT() {}
	~TFunctionCRT() {}
	void prepareTestSample1();
	void run();
};

class TFunctionCRA :
	public TFunctions
{
public:
	TFunctionCRA() {}
	~TFunctionCRA() {}
	void prepareTestSample1();
	void run();
};

