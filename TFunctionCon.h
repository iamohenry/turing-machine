#pragma once
#include "TFunctions.h"

class TFunctionCon1 :
	public TFunctions
{
public:
	TFunctionCon1() {}
	~TFunctionCon1() {}
	void run();	
};

class TFunctionCon2 :
	public TFunctions
{
public:
	TFunctionCon2() {}
	~TFunctionCon2() {}
	void run();
};

class TFunctionCon :
	public TFunctions
{
public:
	TFunctionCon();
	~TFunctionCon();
	void prepareTestSample1();
	void prepareTestSample2();
	void prepareTestSample3();
	void run();
	void test(int order);
};

