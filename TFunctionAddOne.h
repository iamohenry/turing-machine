#pragma once
#include "TFunctions.h"
class TFunctionAddOne :
	public TFunctions
{
public:
	TFunctionAddOne();
	~TFunctionAddOne();
	void run();
private:
	enum eConfig_AddOne {
		eConfig_AddOne_Begin,
		eConfig_AddOne_Increment,
		eConfig_AddOne_rewind,
	};
	eConfig_AddOne m_config;
};

