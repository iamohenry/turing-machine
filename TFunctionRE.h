#pragma once
#include "TFunctions.h"

class TFunctionRE1:
	public TFunctions
{
public:
	TFunctionRE1() {}
	~TFunctionRE1() {}	
	void run();
};

class TFunctionRE :
	public TFunctions
{
public:
	TFunctionRE();
	~TFunctionRE();
	void prepareTestSample1();
	void run();
};


class TFunctionREA :
	public TFunctions
{
public:
	TFunctionREA() {}
	~TFunctionREA() {}
	void prepareTestSample1();
	void run();
};
