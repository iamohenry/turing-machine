#include "TFunctionCP.h"
#include "TFunctionF.h"



TFunctionCP::TFunctionCP()
{
}


TFunctionCP::~TFunctionCP()
{
}

void TFunctionCP::prepareTestSample1()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = '1';
	m_cells[120] = 'b';
}

void TFunctionCP::prepareTestSample2()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[119] = '1';
}

void TFunctionCP::prepareTestSample3()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = '0';
	m_cells[120] = 'b';
}

void TFunctionCP::prepareTestSample4()
{
	m_cells[115] = 'e';
	m_cells[116] = 'e';
	m_cells[117] = '1';
	m_cells[118] = 'a';
	m_cells[119] = '0';
}

void TFunctionCP::run()
{
	TFunctionCP1 tfCP1;
	tfCP1.setFunc(tf_func);
	tfCP1.setFunb(tf_funb);
	tfCP1.setSearchA(tf_replaceB);

	TFunctionF tfF;
	tfF.setFunc(tf_funb);
	tfF.setFunb(tf_fund);
	tfF.setSearchA(tf_replaceB);
	
	TFunctionFL tfFL;
	tfFL.setFunc(&tfCP1);
	tfFL.setFunb(&tfF);
	tfFL.setSearchA(tf_searchA);

	tfFL.run();	
}

void TFunctionCP1::run()
{
	TFunctionCP2 tfCP2;
	tfCP2.setFunc(tf_func);
	tfCP2.setFunb(tf_funb);
	tfCP2.setSearchA(m_cells[m_index]);

	TFunctionFL tfFL;
	tfFL.setFunc(&tfCP2);
	tfFL.setFunb(tf_funb);
	tfFL.setSearchA(tf_searchA);

	tfFL.run();
}

void TFunctionCP2::run()
{
	TFunctions* tfc = static_cast<TFunctions*>(tf_func);
	TFunctions* tfb = static_cast<TFunctions*>(tf_funb);
	if (matchSth(tf_searchA)) {
		if (tfc) {
			tfc->run();
		}
	}
	else {
		if (tfb) {
			tfb->run();
		}
	}
}
