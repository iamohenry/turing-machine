#pragma once
#include "TFunctions.h"

class TFunctionL :
	public TFunctions 
{
public:
	TFunctionL();
	~TFunctionL();
	void run();
};

class TFunctionR :
	public TFunctions
{
public:
	TFunctionR() {}
	~TFunctionR(){}
	void run();
};

class TFunctionF :
	public TFunctions
{
public:
	TFunctionF();
	~TFunctionF();
	void prepareTestSample1();
	void prepareTestSample2();
	void prepareTestSample3();
	void run();
private:
	void f1(void* func, void* funb, char searchA);
	void f2(void* func, void* funb, char searchA);
};

class TFunctionFR :
	public TFunctionF
{
public:
	TFunctionFR();
	~TFunctionFR();
	void run();
};

class TFunctionFL :
	public TFunctionF
{
public:
	TFunctionFL();
	~TFunctionFL();
	void run();
};
