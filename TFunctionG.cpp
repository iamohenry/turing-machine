#include "TFunctionG.h"



TFunctionG::TFunctionG()
{
}


TFunctionG::~TFunctionG()
{
}

void TFunctionG::test(int order)
{
	reset();
	switch (order) {
	case 1:
		prepareTestSample("ee1a1b");
		m_index -= strlen("ee1a1b");
		break;
	case 2:
		prepareTestSample2();
		break;
	case 3:
		prepareTestSample3();
		break;
	case 4:
		prepareTestSample4();
		break;
	default:
		break;
	}
	run();
	sendUpdateSignal();
}

void TFunctionG::prepareTestSample2()
{
	prepareTestSample("ee1a0a1b0b");
	m_index -= strlen("ee1a0a1b0b");
}

void TFunctionG::prepareTestSample3()
{
	m_cells[101] = 'a';
	m_cells[112] = 'e';
	m_cells[113] = 'e';
	m_cells[114] = '1';
	m_cells[116] = '0';
	m_cells[118] = '0';
	m_cells[120] = '1';
	m_index -= 4;
}

void TFunctionG::prepareTestSample4()
{
	m_cells[112] = 'e';
	m_cells[113] = 'e';
	m_cells[114] = '1';
	m_cells[116] = '0';
	m_cells[118] = '0';
	m_cells[120] = '1';
	m_cells[121] = 'a';
	m_cells[122] = '1';
	m_cells[124] = '1';	
	m_index -= 8;
}

void TFunctionG::reset()
{
	TFunctions::reset();
	//m_index = 112;
}

void TFunctionG::run()
{
	while (!matchSth(0)) {
		R();
		sendUpdateSignal();
	}

	if (matchSth(0)) {
		R();
		sendUpdateSignal();
		g1();
	}
}

void TFunctionG::g1()
{
	if (matchSth(0)) {
		if (tf_func) {
			TFunctions* func = static_cast<TFunctions*>(tf_func);
			func->run();
		}
	}
	else {
		R();
		sendUpdateSignal();
		run();
	}
}

void TFunctionGII::test(int order)
{
	reset();
	TFunctionG tfG;
	switch (order) {
	case 1:
		prepareTestSample("ee1a1b");
		m_index -= strlen("ee1a1b");
		break;
	case 2:		
		tfG.prepareTestSample2();
		break;
	case 3:
		//TFunctionG tfG;
		tfG.prepareTestSample3();
		break;
	case 4:
		//TFunctionG tfG;
		tfG.prepareTestSample4();
		break;
	default:
		break;
	}
	run();
	sendUpdateSignal();
}

void TFunctionGII::run()
{
	TFunctionG tfG;
	TFunctionGII_1 tfGII_1;
	tfGII_1.setFunc(tf_func);
	tfGII_1.setSearchA(tf_searchA);
	tfG.setFunc(&tfGII_1);
	tfG.run();
}

void TFunctionGII_1::run()
{
	while (!matchSth(tf_searchA)){
		L();
		sendUpdateSignal();
	} 

	if (matchSth(tf_searchA)) {
		if (tf_func){
			TFunctions* tf = static_cast<TFunctions*>(tf_func);
			tf->run();
		}
	}
}
