#pragma once
#include "TFunctions.h"
class TFunctionSqrtOfFive :
	public TFunctions
{
public:
	TFunctionSqrtOfFive();
	~TFunctionSqrtOfFive();
	void run();
private:
	double calResult();	
private:
	enum eConfig_SqrtOfFive {
		eConfig_SqrtOfFive_begin,
		eConfig_SqrtOfFive_new,
		eConfig_SqrtOfFive_mark_digits,
		eConfig_SqrtOfFive_find_x,
		eConfig_SqrtOfFive_first_r,
		eConfig_SqrtOfFive_last_r,
		eConfig_SqrtOfFive_find_digits,
		eConfig_SqrtOfFive_find_1st_digit,
		eConfig_SqrtOfFive_found_1st_digit,
		eConfig_SqrtOfFive_find_2nd_digit,
		eConfig_SqrtOfFive_found_2nd_digit,
		eConfig_SqrtOfFive_add_zero,
		eConfig_SqrtOfFive_add_one,
		eConfig_SqrtOfFive_carry,
		eConfig_SqrtOfFive_add_finished,
		eConfig_SqrtOfFive_erase_old_x,
		eConfig_SqrtOfFive_print_new_x,
		eConfig_SqrtOfFive_erase_old_y,
		eConfig_SqrtOfFive_print_new_y,
		eConfig_SqrtOfFive_find_tbd_digit,
		eConfig_SqrtOfFive_find_last_r,
		eConfig_SqrtOfFive_check_res,
		eConfig_SqrtOfFive_check_2_nd_digit,
		eConfig_SqrtOfFive_check_3rd_digit,
		eConfig_SqrtOfFive_reset_new_x,
		eConfig_SqrtOfFive_flag_result_digits,
		eConfig_SqrtOfFive_unflag_result_digits,
		eConfig_SqrtOfFive_new_digit_is_zero,
		eConfig_SqrtOfFive_print_zero_digit,
		eConfig_SqrtOfFive_new_digit_is_one,
		eConfig_SqrtOfFive_print_one_digit,
		eConfig_SqrtOfFive_cleanup,
	};
	eConfig_SqrtOfFive m_config;
};

