#pragma once
#include "TFunctions.h"


class TFunctionOv :
	public TFunctions
{
public:
	TFunctionOv() {}
	~TFunctionOv() {}
	void run();
};

class TFunctionInst1 :
	public TFunctions
{
public:
	TFunctionInst1(){}
	~TFunctionInst1() {}
	void run();
};

class TFunctionInst :
	public TFunctions
{
public:
	TFunctionInst();
	~TFunctionInst();
	void run();
};

