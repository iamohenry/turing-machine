#pragma once
#include <qdebug.h>
#include <qvector.h>
#include <qstring.h>
#include <qobject.h>
#include <iostream>

using namespace std;

//typedef void func3(void*, void*, char);
//typedef void func2(void*, char);

const uint maxCellsSize = 1024;

class TFunctions:public QObject
{
	Q_OBJECT
public:	
	TFunctions();
	~TFunctions();
public:
	virtual void run() {};
	void setFunc(void* func);
	void setFunb(void* funb);
	void setFund(void* fund);
	void setFune(void* fune);
	void setSearchA(char search);
	void setReplaceB(char replace);
	void setCharGama(char gama);
	void setCharDelta(char delta);
	void setCharE(char e);
	void reset();
	void resetCells();
	void resetIndex();
	void E();
	void R();
	void L();
	bool matchSth(char a);
	void prSth(char a);
	void prepareTestSample(const char * sampleStr);

	void prepareUMTestSample(const char * sampleStr);
	//void prepareTestSample(char*  sampleStr, int len);
	void prepareTestSample1();
	void prepareTestSample2();
	void prepareTestSample3();
	static TFunctions* getInstance() { return &instance; };
	static void setTestBreak(bool isBreak) { m_isTestBreak = isBreak; };
signals:
	void updateSignal(void);
public:	
	void sendUpdateSignal();
public slots:
	void runCycle(int freq);
protected:
	void *tf_func;
	void *tf_funb;
	void *tf_fund;
	void *tf_fune;
	char tf_searchA;
	char tf_replaceB;
	char tf_char_gama;
	char tf_char_delta;
	char tf_char_e;

public:
	static char m_cells[maxCellsSize];
	static int m_index;
	static int m_prevIndex;
	static TFunctions instance;
	static int m_runDelay;
	static bool m_isTestBreak;
};

class TFunctionFoo :
	public TFunctions
{
public:
	TFunctionFoo();
	~TFunctionFoo();
	void run();
	void setFuncName(QString funcName);
private:
	QString m_funcName;
};

