#include "TFunctionSh.h"
#include "TFunctionF.h"
#include "TFunctionInst.h"
#include "TFunctionPE.h"


TFunctionSh::TFunctionSh()
{
}


TFunctionSh::~TFunctionSh()
{
}

void TFunctionSh::run()
{
	TFunctionSh1 tfSh1;
	TFunctionInst tfInst;
	TFunctionF tfF;
	tfF.setFunc(&tfSh1);
	tfF.setFunb(&tfInst);
	tfF.setSearchA('u');
	tfF.run();
}

void TFunctionSh1::run()
{
	for (int i = 0; i < 3; i++) {
		L();
		sendUpdateSignal();
	}
	TFunctionSh2 tfSh2;
	tfSh2.run();
}

void TFunctionSh2::run()
{
	TFunctionInst tfInst;
	TFunctionSh3 tfSh3;
	if (matchSth('D')) {
		for (int i = 0; i < 4; i++) {
			R();
			sendUpdateSignal();
		}
		tfSh3.run();
	}
	else{
		tfInst.run();
	}
}

void TFunctionSh3::run()
{
	TFunctionInst tfInst;
	TFunctionSh4 tfSh4;
	if (matchSth('C')) {
		R();
		sendUpdateSignal();
		R();
		sendUpdateSignal();
		tfSh4.run();
	}
	else {
		tfInst.run();
	}
}

void TFunctionSh4::run()
{
	TFunctionInst tfInst;
	TFunctionPE2 tfPE2;
	TFunctionSh5 tfSh5;
	if (matchSth('C')) {
		R();
		sendUpdateSignal();
		R();
		sendUpdateSignal();
		tfSh5.run();
	}
	else {
		tfPE2.setFunc(&tfInst);
		tfPE2.setSearchA('0');
		tfPE2.setReplaceB(':');
		tfPE2.run();
	}
}

void TFunctionSh5::run()
{
	TFunctionInst tfInst;
	TFunctionPE2 tfPE2;	
	if (matchSth('C')) {
		tfInst.run();
	}
	else {
		tfPE2.setFunc(&tfInst);
		tfPE2.setSearchA('1');
		tfPE2.setReplaceB(':');
		tfPE2.run();
	}
}
