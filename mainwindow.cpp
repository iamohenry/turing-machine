﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <thread>
#include <QPushButton>
#include <QLabel>
#include <time.h>
#include <iostream>
#include <windows.h>
#include <QDebug>
#include <QGuiApplication>
#include "TFunctions.h"
#include <qobject.h>
#include <runTM.h>
#include <QFont>

using namespace std;

MainWindow* MainWindow::instance = nullptr;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);	
    init();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setCells(char * cell)
{
	m_cells = cell;
}

void MainWindow::setIndex(int *index)
{
	m_curIndex = index;
}

MainWindow * MainWindow::getInstance()
{
	return instance;
}

void MainWindow::setInstance(MainWindow * window)
{
	instance = window;
}

void MainWindow::initBtns()
{	
	for (int i = 0; i < maxBtnSize; i++) {
		m_btns[i] = nullptr;
	}
	btnSize = 0;

	QList<QPushButton*> btns = findChildren<QPushButton*>();
	foreach(QPushButton* btn, btns) {
		QString btnName = btn->objectName();
		qDebug() << "btn's name is " << btnName;
		int btnIndex = btnName.indexOf("pb_");
		if (btnIndex >= 0) {
			QString btnIndexStr = btnName.right(2);
			int index = btnIndexStr.toInt();
			if (index >= 0) {
				qDebug() << "index is " << index;
				m_btns[index] = btn;
				btnSize++;
			}
		}
	}
}

void MainWindow::init()
{
	//connect(TFunctions::getInstance(), &TFunctions::updateSignal, this, &MainWindow::update);
	connect(this, &MainWindow::tuneRunFreq, TFunctions::getInstance(), &TFunctions::runCycle);
	connect(this, &MainWindow::startTestCEA3, this, &MainWindow::testCEA3);
	setCells(TFunctions::getInstance()->m_cells);
	setIndex(&(TFunctions::getInstance()->m_index));
	initIndex();
	initBtns();
	setInstance(this);
}

void MainWindow::initIndex()
{
	for (int i = 0; i < maxBtnSize; i++) {
		m_lbls[i] = nullptr;
	}

	QList<QLabel*> lbls = findChildren<QLabel*>();
	foreach(QLabel* lbl, lbls) {
		QString lblName = lbl->objectName();
		qDebug() << "lbl's name is " << lblName;
		int lblIndex = lblName.indexOf("lbl_");
		if (lblIndex >= 0) {
			QString lblIndexStr = lblName.right(2);
			int index = lblIndexStr.toInt();
			if (index >= 0) {
				qDebug() << "index is " << index;
				m_lbls[index] = lbl;				
			}
		}
	}
}

void MainWindow::updateBtns()
{
    clearBtns();
	for (int i = 0; i < maxBtnSize; i++) {
		if (nullptr == m_btns[i])
			break;
		int index = *m_curIndex - (btnSize / 2) + i;
		if ( (index>= 0)
			&& (*m_curIndex < (maxCellsSize - maxBtnSize))) {
			char btnChar = m_cells[index];
			if (btnChar)
				m_btns[i]->setText(QString(m_cells[index]));
			else
				m_btns[i]->setText("");
			//m_btns[i]->setText(QString((m_cells[index]?(m_cells[index]):(""))));
			if (i - btnSize / 2 == 0) {
				QFont font = m_btns[i]->font();
				font.setPointSize(14);
				font.setBold(true);
				m_btns[i]->setFont(font);
				m_btns[i]->setStyleSheet("color:red;");
			}
		}
	}
}

void MainWindow::updateIndex()
{
    clearIndex();

	for (int i = 0; i < maxBtnSize; i++) {
		if (nullptr == m_lbls[i])
			break;
		int index = *m_curIndex - (btnSize / 2) + i;
#if 0
		if ( (index>= 0)
			&& *m_curIndex < (maxCellsSize - maxBtnSize)) {
#else
		if(*m_curIndex < (maxCellsSize - maxBtnSize)) {
#endif
			m_lbls[i]->setText(QString::number(index));
			if (i - btnSize / 2 == 0) {
				QFont font = m_lbls[i]->font();
				font.setPointSize(12);
				font.setBold(true);
				m_lbls[i]->setFont(font);
				m_lbls[i]->setStyleSheet("color:red;");
			}
		}
	}
}

void MainWindow::update()
{
    updateBtns();
    updateIndex();
	qApp->processEvents(); //需要包含qApp的头文件	
}

void MainWindow::clearBtns()
{
    QList<QPushButton*> btns = findChildren<QPushButton*>();
    foreach(QPushButton* btn ,btns){		
		if (btn->objectName().contains("pb_")) {
			btn->setText("");
		}
    }
#if 0
	ui->pbRunF->setText("RUN_FUNC_F");
	ui->pbRunE->setText("RUN_FUNC_E");
	ui->pbRunG0101->setText("RUN_G0101");
	ui->pbRunTD->setText("RUN_TD");
#endif
}

void MainWindow::clearIndex()
{
#if 0
    QList<QLabel*> lbs = findChildren<QLabel*>();
    foreach(QLabel* lb ,lbs){
        lb->setText("");
    }
#endif


	QList<QLabel*> lbs = findChildren<QLabel*>();
	foreach(QLabel* lb, lbs) {
		if (lb->objectName().contains("lb_")) {
			lb->setText("");
		}
	}
}

void MainWindow::clearRWIndex() {

}

void MainWindow::keyReleaseEvent(QKeyEvent * event)
{
	char test = 0;
	switch (event->key())
	{
	case Qt::Key_Return:
		TFunctions::getInstance()->setTestBreak(false);
		break;
	default:
		break;
	}
}

void MainWindow::on_pbRunE_released()
{
	runTM run;
	run.testTFE();
}

void MainWindow::on_pbRunG0101_released()
{
	runTM run;
	run.testG0101();
}

void MainWindow::on_pbRunTD_released()
{
	runTM run;
	run.testTD();
}

void MainWindow::on_pbContinue_pressed() {
	TFunctions::getInstance()->setTestBreak(false);
}

void MainWindow::on_pbContinue_released() {
	TFunctions::getInstance()->setTestBreak(false);
}

void MainWindow::on_pbRunF_released() {
	runTM run;
	run.testTFF();
}

void MainWindow::on_pbRunAddOne_released()
{
	runTM run;
	run.testAddOne();
}

void MainWindow::on_pbRunSqrtOfTwo_released()
{
	runTM run;
	run.testSqrtOfTwo();
}

void MainWindow::on_pbRunEA_released()
{
	runTM run;
	run.testTFEA();
}

void MainWindow::on_pbRunPE_released()
{
	runTM run;
	run.testTFPE();
}

void MainWindow::on_pbRunFR_released()
{
	runTM run;
	run.testTFFR();
}

void MainWindow::on_pbRunFL_released()
{
	runTM run;
	run.testTFFL();
}

void MainWindow::on_pbRunC_released()
{
	runTM run;
	run.testTFC();
}

void MainWindow::on_pbRunCE_released()
{
	runTM run;
	run.testTFCE();
}

void MainWindow::on_pbRunCEA_released()
{
	runTM run;
	run.testTFCEA();
}

void MainWindow::on_pbRunRE_released()
{
	runTM run;
	run.testTFRE();
}

void MainWindow::on_pbRunREA_released()
{
	runTM run;
	run.testTFREA();
}

void MainWindow::on_pbRunCR_released()
{
	runTM run;
	run.testTFCR();
}

void MainWindow::on_pbRunCRA_released()
{
	runTM run;
	run.testTFCRA();
}

void MainWindow::on_pbRunASCII_released()
{
	runTM run;
	run.testASCII();
}

void MainWindow::on_pbRunCP_released()
{
	runTM run;
	run.testTFCP();
}

void MainWindow::on_pbRunCPE_released()
{
	runTM run;
	run.testTFCPE();
}

void MainWindow::on_pbRunCPEA_released()
{
	runTM run;
	run.testTFCPEA();
}

void MainWindow::on_pbRunG_released()
{
	runTM run;
	run.testTFG();
}

void MainWindow::on_pbRunGII_released()
{
	runTM run;
	run.testTFGII();
}

void MainWindow::on_pbRunPE2_released()
{
	runTM run;
	run.testTFPE2();
}

void MainWindow::on_pbRunCEA2_released()
{
	runTM run;
	run.testTFCEA2();
}
void MainWindow::testCEA3() {
	runTM run;
	run.testTFCEA3();
}

void MainWindow::on_pbRunCEA3_released()
{
#if 0
	emit startTestCEA3();
#if 0
	std::thread test([] {
		runTM run;
		run.testTFCEA3();
	});
#endif
	//std::thread test =std::thread(testCEA3);
	//test.join();
#else
	runTM run;
	run.testTFCEA3();
#endif
}

void MainWindow::on_hs_runfreq_valueChanged(int value)
{
	emit tuneRunFreq(value);
}

void MainWindow::on_pbRunEM_released()
{
	runTM run;
	run.testTFEM();
}

void MainWindow::on_pbRunCon_released()
{
	runTM run;
	run.testTFCon();
}

void MainWindow::on_pbRunB_released()
{
	runTM run;
	run.testTFB();
}

void MainWindow::on_pbRunSqrtOfThree_released()
{
	runTM run;
	run.testTFSqrt3();
}

void MainWindow::on_pbRunSqrtOfFive_released()
{
	runTM run;
	run.testTFSqrt5();

}

